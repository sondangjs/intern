-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 01, 2019 at 02:44 PM
-- Server version: 10.2.25-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u8206540_absensi2`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `kode_otp` varchar(6) NOT NULL,
  `last_send` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `from_users_id` int(11) NOT NULL,
  `to_users_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `sentat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registerDemo`
--

CREATE TABLE `registerDemo` (
  `name` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registerDemo`
--

INSERT INTO `registerDemo` (`name`, `username`, `password`, `hobby`) VALUES
('coba', 'coba', 'coba', 'coab');

-- --------------------------------------------------------

--
-- Table structure for table `satu_users`
--

CREATE TABLE `satu_users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `gender` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satu_users`
--

INSERT INTO `satu_users` (`id`, `name`, `email`, `password`, `gender`) VALUES
(1, 'khee', 'rizujikeda@gmail.com', '1aba7debd9d8dc4a560d718df3cbbd8b', 'male'),
(2, 'iwebnet', 'iwebnet.id@gmail.com', '1aba7debd9d8dc4a560d718df3cbbd8b', 'female');

-- --------------------------------------------------------

--
-- Table structure for table `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `judul_berita` varchar(255) NOT NULL,
  `tanggal_posting` date NOT NULL,
  `isi_berita` text NOT NULL,
  `penulis` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_berita`
--

INSERT INTO `tb_berita` (`id`, `foto`, `judul_berita`, `tanggal_posting`, `isi_berita`, `penulis`) VALUES
(1, 'foto_1.jpg', 'Crisitian Gonzales Percaya Madura United Lebih Baik Di Liga 1', '2018-02-08', 'Madura United tersingkir dari Piala Presiden di babak delapan besar.</br></br>\r\nKegagalan Madura United di Piala Presiden 2018, tak membuat Cristian Gonzales, jadi pesemitis ketika Liga 1 bergulir. Ia menilai, timnya bakal bisa berbicara banyak ketika kompetisi kasta teratas digelar.\r\n</br></br>\r\nLangkah Madura United dalam Piala Presiden terhenti di babak delapan besar. Pasukan Gomes de Oliviera tersebut dikalahkan Bali United lewat babak adu penalti di Stadion Manahan, Solo, akhir pekan lalu.\r\n</br></br>\r\nNamun, Gonzales merasa timnya tidak kalah kelas dibandingkan Bali United. Menurutnya, Madura United takluk karena tidak dinaungi dewi fortuna saja pada laga tersebut.\r\n</br></br>\r\n\"Waktu di Piala Presiden kami sebenarnya sudah bagus, meskipun lawan kalah lewat penalti. Ya sekarang kami harus lupakan itu dan konsentrasi untuk kompetisi nanti,\" ucap Gonzales.\r\n</br></br>\r\nLebih jauh, eks Arema FC tersebut menuturkan belum bisa memprediksikan lawan tangguh yang bakal dihadapai Madura United pada Liga 1 2018. Ini lantaran, ia menyebutkan seluruh kontestan pasti ingin meraih hasil maksimal.\r\n</br></br>\r\n\"Adanya Piala Presiden ini sangat bagus sebagai sarana kami untuk menilai semua tim, tetapi pasti saat kompetisi dimulai nanti situasi akan berbeda,\" ujarnya. ', 'Muhammad Ridwan'),
(2, 'foto_2.jpg', 'REVIEW Eredivisie Belanda: PSV Mantap Di Puncak, NAC Tinggalkan Degradasi', '2018-02-08', 'Kemenangan tipis PSV cukup bagi mereka mempertahankan posisi puncak, diikuti Ajax di urutan kedua setelah menang 4-2.\r\n</br></br>\r\nAZ Alkmaar mengukuhkan posisi ketiga dengan menghancurkan tuan rumah Twente, yang mengakhiri laga dengan sepuluh pemain, 4-0.\r\n</br></br>\r\nTim tamu membuka keunggulan di menit kesepuluh via penalti Alireza Jahanbakhsh. Twente berhasil mengimbangi permainan AZ sampai mereka harus kehilangan Cristian Cuevas yang diusir wasit setelah mendapat dua kartu kuning di menit ke-73.\r\n</br></br>\r\nMemasuki pengujung laga, AZ menambah tiga gol, dua di antaranya dicetak Wout Weghorst sebelum Jahanbakhsh mencetak bracenya di injury time.', 'DEWI AGRENIAWATI'),
(3, 'foto_3.jpg', 'Selasa Ini, Belanda Tunjuk Ronald Koeman Sebagai Pelatih', '2018-02-09', 'Koeman yang sudah tidak lagi bekerja sejak didepak Everton pada Oktober kemarin akan segera diresmikan sebagai pelatih Belanda.\r\n</br></br>\r\nMedia Inggris Sky Sports News menyebut Belanda akan mengangkat Ronald Koeman sebagai pelatih baru mereka pada Selasa (6/2) ini.\r\n</br></br>\r\nKoeman, 54 tahun, sudah tidak lagi memiliki pekerjaan sejak dilengserkan Everton dari kursinya sebagai manajer pada Oktober silam menyusul rentetan hasil buruk yang diraih di awal musim.\r\n</br></br>\r\nBelanda sendiri juga belum memiliki pelatih sejak ditinggal Dick Advocaat yang memutuskan mundur pascakegagalan mengantar negaranya lolos ke Piala Dunia 2018.', 'Adhe Makayasa\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(23) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `unique_id`, `nama`, `email`, `encrypted_password`, `salt`) VALUES
(1, '5b9f233f4b3a48.20154459', 'rizki', 'rizujikeda@gmail.com', 'hXpd7moqRLjXI2mqwhRG6pkvsZk1ZGNlMTYxZGU4', '5dce161de8');

-- --------------------------------------------------------

--
-- Table structure for table `userabsensi`
--

CREATE TABLE `userabsensi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `UserLocationTable`
--

CREATE TABLE `UserLocationTable` (
  `id` int(50) NOT NULL,
  `user_lat` decimal(10,8) NOT NULL,
  `user_lng` decimal(11,8) NOT NULL,
  `user_address` varchar(200) NOT NULL,
  `DateTime` varchar(255) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserLocationTable`
--

INSERT INTO `UserLocationTable` (`id`, `user_lat`, `user_lng`, `user_address`, `DateTime`, `image`) VALUES
(337, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 26, 2019 18:01:51', ''),
(336, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 26, 2019 18:01:12', ''),
(335, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 26, 2019 17:59:57', ''),
(334, '3.60860540', '98.72695290', 'Citraland Bagya City, Kenangan, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 26, 2019 17:58:50', ''),
(338, '3.61956060', '98.72764100', 'Jl. Sukarela Bar. No.31, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 12:51:14', 'http://yukmarry.com/absensi/upload/.png'),
(339, '3.61610190', '98.72901740', 'Jalan Pertahanan#1C komplek Veteran, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 13:04:09', 'http://yukmarry.com/absensi/upload/.png'),
(340, '3.61956060', '98.72764100', 'Jl. Sukarela Bar. No.31, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 13:10:04', 'http://yukmarry.com/absensi/upload/.png'),
(341, '3.61641180', '98.72936150', 'Jl. Kapten Batu Sihombing, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 13:13:54', ''),
(342, '3.61609860', '98.72936150', 'Jl. Legiun Veteran No.3A, Kenangan, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 13:20:28', ''),
(343, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 14:48:29', ''),
(344, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 14:51:12', ''),
(345, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 14:52:33', ''),
(346, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 14:53:56', ''),
(347, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 14:59:58', ''),
(348, '0.00000000', '0.00000000', '', '', 'http://yukmarry.com/absensi/upload/347.png'),
(349, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 15:31:48', ''),
(350, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 15:35:45', ''),
(351, '3.61693820', '98.72864170', 'Jl. Vetpur Raya III, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 15:39:03', ''),
(352, '0.00000000', '0.00000000', '', '', 'http://yukmarry.com/absensi/upload/351.png'),
(353, '3.61610190', '98.72901740', 'Jalan Pertahanan#1C komplek Veteran, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 15:42:08', ''),
(354, '0.00000000', '0.00000000', '', '', 'http://yukmarry.com/absensi/upload/353.png'),
(355, '3.61641180', '98.72936150', 'Jl. Kapten Batu Sihombing, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 15:50:09', ''),
(356, '3.61641180', '98.72936150', 'Jl. Kapten Batu Sihombing, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 15:58:48', ''),
(357, '0.00000000', '0.00000000', '', '', 'http://yukmarry.com/absensi/upload/356.png'),
(358, '3.61610190', '98.72901740', 'Jalan Pertahanan#1C komplek Veteran, Tembung, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371, Indonesia', 'Jun 27, 2019 16:03:20', 'http://yukmarry.com/absensi/upload/358.png');

-- --------------------------------------------------------

--
-- Table structure for table `UserLoginTable`
--

CREATE TABLE `UserLoginTable` (
  `id` int(11) NOT NULL,
  `complete_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_password` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserLoginTable`
--

INSERT INTO `UserLoginTable` (`id`, `complete_name`, `phone_number`, `user_email`, `user_password`) VALUES
(1, 'rizki', '081218605405', 'rizujikeda@gmail.com', 'watergate16'),
(12, 'Rizki Mayandi Hasibuan', '08100100100', 'khee_itb@yahoo.com', 'watergate'),
(13, 'Asmaniar', '081260029016', 'asmaniar@yahoo.com', 'watergate1'),
(14, 'Sukry Asdar Putra', '0812100100', 'sukry@yahoo.com', 'watergate'),
(15, 'Ricki mayandi', '085656100100', 'coba@gmail.com', 'watergate'),
(18, 'daftar baru', '082360965053', 'iwebnet.id@gmail.com', 'password'),
(17, 'Asmaniar', '0812600290162', 'asmaniar@yahoo.com', 'watergate'),
(19, 'A51 oppo', '0813900900', 'lampuislamnikah@gmail.com', 'watergate'),
(20, 'rizuik', '081289000', 'dsfsd@dfgdf', 'watergate'),
(31, 'Rudy Sutadi', '082211996000', 'rudysutadi@kidaba.com', '123456'),
(48, '', '', '', ''),
(47, 'titik', '0896100100', 'titik@gmail.com', 'watergate');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `sno` int(11) NOT NULL,
  `unique_id` varchar(23) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `encrypted_password` varchar(256) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_users_from` (`to_users_id`),
  ADD KEY `messages_users_to` (`from_users_id`);

--
-- Indexes for table `registerDemo`
--
ALTER TABLE `registerDemo`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `satu_users`
--
ALTER TABLE `satu_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_id` (`unique_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `userabsensi`
--
ALTER TABLE `userabsensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `UserLocationTable`
--
ALTER TABLE `UserLocationTable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `UserLoginTable`
--
ALTER TABLE `UserLoginTable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`sno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `satu_users`
--
ALTER TABLE `satu_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_berita`
--
ALTER TABLE `tb_berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `UserLocationTable`
--
ALTER TABLE `UserLocationTable`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=359;

--
-- AUTO_INCREMENT for table `UserLoginTable`
--
ALTER TABLE `UserLoginTable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
