package id.loginkuid.absensi.authentication;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import id.loginkuid.absensi.R;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.wildma.idcardcamera.camera.CameraActivity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RegistrasiActivity extends AppCompatActivity {

    private static final String TAG = "registrasi-activity" ;
    Button btnIDCard, toSelfie;

    private ImageView selfieImage;

    CardView toRegis;
    private EditText etNamaLengkap, etNIK, etEmail, etPassword;

    private TextView OCRTextView;

    private static final int REQUEST_CODE = 101;

    private ImageView scannedKTPView;

    private TextView detectedTextView;
    private Uri imageUri;
    private static final int REQUEST_CAMERA = 1;
    private static final int MY_PERMISSIONS_REQUESTS = 0;

    String path, path2;
    Bitmap bitmap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        //TO not display keyboard on activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        btnIDCard = findViewById(R.id.btnIDCard);
        toSelfie = findViewById(R.id.toSelfie);
        toRegis = findViewById(R.id.toRegis);
        etNamaLengkap =findViewById(R.id.etNamaLengkap);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPasssword);
        etNIK = findViewById(R.id.etNIK);

        detectedTextView = (TextView) findViewById(R.id.detected_text);
        detectedTextView.setMovementMethod(new ScrollingMovementMethod());

        requestPermissions();

//        OCRTextView = findViewById(R.id.OCRTextView);

        String namaLengkap = etNamaLengkap.getText().toString();
        String NIK = etNIK.getText().toString();


        toRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(RegistrasiActivity.this, PhoneVerifyActivity.class);
//                startActivity(intent);
//                finish();
            }
        });

        selfieImage =findViewById(R.id.previewImage);
        scannedKTPView = findViewById(R.id.scannedKTPView);

//        Intent intent = getIntent();
//        path2 = intent.getStringExtra("IMAGE_PATH_SELFIE2");
//
//        if (path2 != null) {
//
//            toSelfie.setTextColor(getResources().getColor(R.color.BLUE));
//            toSelfie.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ico_check, 0, 0, 0);
//            selfieImage.setVisibility(View.VISIBLE);
//            selfieImage.setImageBitmap(BitmapFactory.decodeFile(path2));
//            scannedKTPView.setImageBitmap(BitmapFactory.decodeFile(path));
//        }
//
////        else if (path != null){
////            scannedKTPView.setImageBitmap(BitmapFactory.decodeFile(path));
////        }
//        else{
////            selfieImage.setVisibility(View.GONE);
//            scannedKTPView.setVisibility(View.GONE);
//        }


//        findViewById(R.id.btnIDCard).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String filename = System.currentTimeMillis() + ".jpg";
//
//                ContentValues values = new ContentValues();
//                values.put(MediaStore.Images.Media.TITLE, filename);
//                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
//                imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                Intent intent = new Intent();
//                intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//                startActivityForResult(intent, REQUEST_CAMERA);
//            }
//        });



        btnIDCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CameraActivity.toCameraActivity(RegistrasiActivity.this, CameraActivity.TYPE_IDCARD_FRONT);
//                finish();
            }
        });

        toSelfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RegistrasiActivity.this, SelfieCameraActivity3.class);

                startActivityForResult(intent, SelfieCameraActivity3.REQUEST_CODE_SELFIE);
//                startActivity(intent);
//                finish();
            }
        });



    }

    private void requestPermissions()
    {
        List<String> requiredPermissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(Manifest.permission.CAMERA);
        }

        if (!requiredPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    requiredPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUESTS);
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
////            path = CameraActivity.getImagePath(data);
////            lang = path + ".traineddata";
//            case REQUEST_CAMERA:
//                if (resultCode == RESULT_OK) {
//                    if (imageUri != null) {
//                        scannedKTPView.setVisibility(View.VISIBLE);
//                btnIDCard.setTextColor(getResources().getColor(R.color.BLUE));
//                btnIDCard.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ico_check, 0, 0, 0);
////                scannedKTPView.setImageBitmap(BitmapFactory.decodeFile(imageUri))
//                scannedKTPView.setImageURI(imageUri);
//
//                        inspect(imageUri);
//                    }
//                }
//                break;
//            default:
//                super.onActivityResult(requestCode, resultCode, data);
//                break;
//        }
//    }
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == CameraActivity.REQUEST_CODE && resultCode == CameraActivity.RESULT_CODE) {
        //获取图片路径，显示图片
        path = CameraActivity.getImagePath(data);
        Log.d(TAG, "PATH =>" + path);

        if (!TextUtils.isEmpty(path)) {
            scannedKTPView.setVisibility(View.VISIBLE);
            btnIDCard.setTextColor(getResources().getColor(R.color.BLUE));
            btnIDCard.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ico_check, 0, 0, 0);
            scannedKTPView.setImageBitmap(BitmapFactory.decodeFile(path));

//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inSampleSize = 4;
//
//            bitmap = BitmapFactory.decodeFile(path);

            //to OCR from KTP image
//            inspectFromBitmap(bitmap);
//            inspect(Uri.parse(path));

//                bitmap = ((BitmapDrawable)scannedKTPView.getDrawable()).getBitmap();

//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inSampleSize = 4;
//
//                bitmap = BitmapFactory.decodeFile(path, options);


        }

    }
    else if (requestCode == SelfieCameraActivity3.REQUEST_CODE_SELFIE && resultCode == SelfieCameraActivity3.RESULT_CODE_SELFIE){
        path = SelfieCameraActivity3.getImagePath(data);
        Log.d(TAG, "PATH =>" + path);
        if (!TextUtils.isEmpty(path)) {
            String result = data.getStringExtra("IMAGE_PATH_SELFIE");
            Log.d(TAG, "PATH =>" + result);

            toSelfie.setTextColor(getResources().getColor(R.color.BLUE));
            toSelfie.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ico_check, 0, 0, 0);
            selfieImage.setVisibility(View.VISIBLE);
            selfieImage.setImageBitmap(BitmapFactory.decodeFile(result));

            bitmap = BitmapFactory.decodeFile(result);
            Log.d(TAG, "PATH =>" + bitmap);
        }


    }

}

    private void inspect(Uri uri) {
        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = getContentResolver().openInputStream(uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inSampleSize = 2;
            options.inScreenDensity = DisplayMetrics.DENSITY_LOW;
            bitmap = BitmapFactory.decodeStream(is, null, options);

            inspectFromBitmap(bitmap);
        } catch (FileNotFoundException e) {
            Log.w(TAG, "Failed to find the file: " + uri, e);
        } finally {
            if (bitmap != null) {
                bitmap.recycle();
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Log.w(TAG, "Failed to close InputStream", e);
                }
            }
        }
    }

    private void inspectFromBitmap(Bitmap bitmap) {
        TextRecognizer textRecognizer = new TextRecognizer.Builder(this).build();
        try {
            if (!textRecognizer.isOperational()) {
                new AlertDialog.
                        Builder(this).
                        setMessage("Text recognizer could not be set up on your device").show();
                return;
            }

            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            SparseArray<TextBlock> origTextBlocks = textRecognizer.detect(frame);
            List<TextBlock> textBlocks = new ArrayList<>();
            for (int i = 0; i < origTextBlocks.size(); i++) {
                TextBlock textBlock = origTextBlocks.valueAt(i);
                textBlocks.add(textBlock);
            }
            Collections.sort(textBlocks, new Comparator<TextBlock>() {
                @Override
                public int compare(TextBlock o1, TextBlock o2) {
                    int diffOfTops = o1.getBoundingBox().top - o2.getBoundingBox().top;
                    int diffOfLefts = o1.getBoundingBox().left - o2.getBoundingBox().left;
                    if (diffOfTops != 0) {
                        return diffOfTops;
                    }
                    return diffOfLefts;
                }
            });

            StringBuilder detectedText = new StringBuilder();
            for (TextBlock textBlock : textBlocks) {
                if (textBlock != null && textBlock.getValue() != null) {
                    detectedText.append(textBlock.getValue());
                    detectedText.append("\n");
                }
            }

            //Output from image to text
            detectedTextView.setText(detectedText);
        }
        finally {
            textRecognizer.release();
        }
    }







//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_CODE) {
//            if(resultCode == Activity.RESULT_OK) {
//                if(null != data && null != data.getExtras()) {
//
//                    String filePath = data.getExtras().getString(ScanConstants.SCANNED_RESULT);
//                    Bitmap baseBitmap = ScanUtils.decodeBitmapFromFile(filePath, ScanConstants.IMAGE_NAME);
//                    scannedImageView.setVisibility(View.VISIBLE);
//                    scannedImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                    scannedImageView.setImageBitmap(baseBitmap);
//                }
//            } else if(resultCode == Activity.RESULT_CANCELED) {
//                finish();
//            }
//        }
//    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }
}
