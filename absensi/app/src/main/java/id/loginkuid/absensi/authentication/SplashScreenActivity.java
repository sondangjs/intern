package id.loginkuid.absensi.authentication;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Arrays;

import id.loginkuid.absensi.R;

public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = "splash-screen";
    private final int SPLASH_TIME = 3000; // in ms

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*
        Set application version
         */
        try {
            String packageName = getPackageName();
            String version = getPackageManager().getPackageInfo(packageName, 0).versionName;
//            TextView applicationVersion = (TextView) findViewById(R.id.app_version);
//            applicationVersion.setText(getString(R.string.str_version) + " " + version);
        } catch (PackageManager.NameNotFoundException error) {
            Log.e("ERROR", Arrays.toString(error.getStackTrace()));
        }

        /*
        Prepare waiting thread
         */
        final Thread thread = new Thread() {
            public void run() {
                /*
                Start waiting counter
                 */
                int wait = 0;
                try {
                    while (SPLASH_TIME > wait) {
                        sleep(100);
                        wait += 100;
                    }
                } catch (InterruptedException e) {
                    /*
                    Log error message
                     */
                    Log.e(TAG, e.getMessage());
                } finally {
                    /*
                    Go to Sign-In page
                     */

                    startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };


        thread.start();


//        if (username != null) {
//            /*
//            If user already sign-in, go to main Page
//             */
//            startActivity(new Intent(this, MainActivity.class));
//            finish();
//        } else {
//            /*
//            If not, execute waiting thread
//             */
//            thread.start();
//        }
    }
}
