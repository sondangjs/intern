package id.loginkuid.absensi.retrofit;

public class UtilsApi {
    public static final String BASE_URL_API =  "http://192.168.43.167/absensi-php-backend-master/";
    public static APIInterface getAPIService(){
        return APIClient.getClient(BASE_URL_API).create(APIInterface.class);
    }
}
