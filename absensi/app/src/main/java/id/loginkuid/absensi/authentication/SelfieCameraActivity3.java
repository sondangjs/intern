package id.loginkuid.absensi.authentication;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
//import android.renderscript.RenderScript;
import android.support.v8.renderscript.RenderScript;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.cameraview.CameraView;
import com.google.android.cameraview.CameraViewImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;

import id.loginkuid.absensi.BuildConfig;
import id.loginkuid.absensi.R;
import io.github.silvaren.easyrs.tools.Nv21Image;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.google.android.cameraview.CameraView.FACING_FRONT;

public class SelfieCameraActivity3 extends AppCompatActivity implements View.OnClickListener {

    public static final int REQUEST_CODE_SELFIE = 1 ;
    public static final int RESULT_CODE_SELFIE = 2;
    private static final int PERMISSION_CODE_STORAGE = 3001;
    private static final int PERMISSION_CODE_CAMERA = 3002;


    private static final String TAG = "Selfi-activity";

    CameraView cameraView;

    ImageView shutterEffect;
    ImageView captureButton;
    ImageView turnButton;

    private View mLlCameraResult;

    private RenderScript rs;

    ImageView imageView3;

    String filePath;

    private boolean frameIsProcessing = false;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfie_camera3);

        cameraView = findViewById(R.id.camera_view);
//        shutterEffect = findViewById(R.id.shutter_effect);
        captureButton = findViewById(R.id.shutter);
        turnButton = findViewById(R.id.turn);
        imageView3 = findViewById(R.id.imageView3);

        mLlCameraResult = findViewById(R.id.ll_camera_result);

        findViewById(R.id.iv_camera_result_ok).setOnClickListener(this);
        findViewById(R.id.iv_camera_result_cancel).setOnClickListener(this);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraView.getAutoFocus();
                cameraView.takePicture();
//                takePhoto();
            }
        });



        cameraView.setFacing(FACING_FRONT);


        rs = RenderScript.create(this);


    }

//    /**
//     * 拍照
//     */
//    private void takePhoto() {
//        cameraView.setEnabled(false);
//        CameraUtils.getCamera().setOneShotPreviewCallback(new Camera.PreviewCallback() {
//            @Override
//            public void onPreviewFrame(final byte[] bytes, Camera camera) {
//                final Camera.Size size = camera.getParameters().getPreviewSize(); //获取预览大小
//                camera.stopPreview();
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        final int w = size.width;
//                        final int h = size.height;
//                        Bitmap bitmap = ImageUtils.getBitmapFromByte(bytes, w, h);
//                        setCropLayout(bitmap);
//                    }
//                }).start();
//            }
//        });
//    }


    @Override
    protected void onResume() {
        super.onResume();
        if (PermissionUtils.isStorageGranted(this) && PermissionUtils.isCameraGranted(this)) {
            cameraView.start();
            setupCameraCallbacks();
        } else {
            if (!PermissionUtils.isCameraGranted(this)) {
                PermissionUtils.checkPermission(this, Manifest.permission.CAMERA,
                        PERMISSION_CODE_CAMERA);
            } else {
                PermissionUtils.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        PERMISSION_CODE_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE_STORAGE:
            case PERMISSION_CODE_CAMERA:
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
        if (requestCode != PERMISSION_CODE_STORAGE && requestCode != PERMISSION_CODE_CAMERA) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    private void setupCameraCallbacks() {
        cameraView.setOnPictureTakenListener(new CameraViewImpl.OnPictureTakenListener() {
            @Override
            public void onPictureTaken(Bitmap bitmap, int rotationDegrees) {
//                cameraView.setVisibility(View.GONE);


                startSavingPhoto(bitmap, rotationDegrees);
//                startSavingPhoto(bitmap, rotationDegrees);


            }
        });

//        cameraView.setOnFocusLockedListener(new CameraViewImpl.OnFocusLockedListener() {
//            @Override
//            public void onFocusLocked() {
//                playShutterAnimation();
//            }
//        });

        cameraView.setOnTurnCameraFailListener(new CameraViewImpl.OnTurnCameraFailListener() {
            @Override
            public void onTurnCameraFail(Exception e) {
                Toast.makeText(SelfieCameraActivity3.this, "Switch Camera Failed. Does you device has a front camera?",
                        Toast.LENGTH_SHORT).show();
            }
        });
        cameraView.setOnCameraErrorListener(new CameraViewImpl.OnCameraErrorListener() {
            @Override
            public void onCameraError(Exception e) {
                Toast.makeText(SelfieCameraActivity3.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        cameraView.setOnFrameListener(new CameraViewImpl.OnFrameListener() {
            @Override
            public void onFrame(final byte[] data, final int width, final int height, int rotationDegrees) {
                if (frameIsProcessing) return;
                frameIsProcessing = true;
                Observable.fromCallable(new Callable<Bitmap>() {
                    @Override
                    public Bitmap call() throws Exception {
                        return Nv21Image.nv21ToBitmap(rs, data, width, height);
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Bitmap>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Bitmap frameBitmap) {
                                if (frameBitmap != null) {
                                    Log.i("onFrame", frameBitmap.getWidth() + ", " + frameBitmap.getHeight());
                                }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                frameIsProcessing = false;
                            }
                        });
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_camera_result_ok) {
//             startSavingPhoto(bitmap);
            final Bitmap bitmap = ((BitmapDrawable)imageView3.getDrawable()).getBitmap();
            Log.d(TAG, "PATH =>" +bitmap);

            Observable.fromCallable(new Callable<Bitmap>() {
                @Override
                public Bitmap call() throws Exception {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(-180);
                    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                }
            }).map(new Function<Bitmap, String>() {
                @Override
                public String apply(Bitmap bitmap) throws Exception {
                    return bitmapToFile(bitmap);
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<String>() {
                        @Override
                        public void accept(String filePath) throws Exception {
                            if (filePath.isEmpty()) {
                                Toast.makeText(SelfieCameraActivity3.this, "Save image file failed :(", Toast.LENGTH_SHORT).show();
                            } else {
                                toGallery(filePath);
                            }
                        }
                    });



        } else if (id == R.id.iv_camera_result_cancel) {
//            cameraView.setVisibility(View.VISIBLE);
            captureButton.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.GONE);
//            imageView3.setImageBitmap(BitmapFactory.decodeFile(filePath));
            mLlCameraResult.setVisibility(View.GONE);

        }
    }

//    private void reviewImage(final Bitmap bitmap, final int rotationDegrees) {
//
//
//        Intent intent = new Intent(this, PreviewSelfieActivity.class);
//
//        intent.putExtra();
//        startActivity(intent);
////        startActivityForResult(intent, PreviewSelfieActivity.REQUEST_CODE_SELFIE);
////
////        mLlCameraResult.setVisibility(View.VISIBLE);
////        captureButton.setVisibility(View.GONE);
//////        imageView3.setVisibility(View.GONE);
////        imageView3.setImageBitmap(bitmap);
////        Log.d(TAG, "PATH =>" +bitmap);
//
////        startSavingPhoto(bitmap, rotationDegrees);
//    }


    private void playShutterAnimation() {
        shutterEffect.setVisibility(View.VISIBLE);
        shutterEffect.animate().alpha(0f).setDuration(300).setListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        shutterEffect.setVisibility(View.GONE);
                        shutterEffect.setAlpha(0.8f);
                    }
                });
    }

    private String bitmapToFile(Bitmap bitmap) {
        //create a file to write bitmap data
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String fileName = getString(R.string.app_name) + sdf.format(currentTime) + ".jpg";
        File file = new File(Environment.getExternalStoragePublicDirectory("tandatanganid"), fileName);
        try {
            file.createNewFile();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
            return "";
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, bos);
        byte[] bitmapData = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
            return "";
        } finally {
            try {
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file.getAbsolutePath();
    }

    private void startSavingPhoto(final Bitmap bitmap, final int rotationDegrees) {



        Observable.fromCallable(new Callable<Bitmap>() {
            @Override
            public Bitmap call() throws Exception {
                Matrix matrix = new Matrix();
                matrix.postRotate(-rotationDegrees);
                return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
        }).map(new Function<Bitmap, String>() {
            @Override
            public String apply(Bitmap bitmap) throws Exception {
                return bitmapToFile(bitmap);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String filePath) throws Exception {
                        if (filePath.isEmpty()) {
                            Toast.makeText(SelfieCameraActivity3.this, "Save image file failed :(", Toast.LENGTH_SHORT).show();
                        } else {
                            notifyGallery(filePath);
                        }
                    }
                });
    }

    public static String getImagePath(Intent data) {
        if (data != null) {
            return data.getStringExtra("IMAGE_PATH_SELFIE");
        }
        return "";
    }

    private void notifyGallery(String filePath) {

        Log.d(TAG,"filepath => 1 on notifyGallery =>" +filePath);

        cameraView.setVisibility(View.GONE);
        captureButton.setVisibility(View.GONE);
        imageView3.setImageBitmap(BitmapFactory.decodeFile(filePath));
        mLlCameraResult.setVisibility(View.VISIBLE);

//        toGallery(filePath);

//        startActivity(intent);
//        finish();
    }

    private void toGallery(String filePath) {

        Log.d(TAG,"filepath => 2 on togallery =>" +filePath);

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(filePath);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);

        Intent intent =new Intent();
        mediaScanIntent.putExtra("IMAGE_PATH_SELFIE", filePath);
        setResult(RESULT_CODE_SELFIE, mediaScanIntent);
//        intent.putExtra("IMAGE_PATH_SELFIE", filePath);
//        setResult(REQUEST_CODE_SELFIE, intent);
        finish();
    }

}
