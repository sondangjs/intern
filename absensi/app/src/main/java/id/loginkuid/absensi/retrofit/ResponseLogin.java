package id.loginkuid.absensi.retrofit;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin {

    @SerializedName("kode")
    private int kode;
    @SerializedName("pesan")
    private String pesan;
    @SerializedName("user")
    private User user;

    public ResponseLogin(int kode, String pesan, User user) {
        this.kode = kode;
        this.pesan = pesan;
        this.user = user;
    }

    public int getKode() {
        return kode;
    }

    public void setKode(int kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
