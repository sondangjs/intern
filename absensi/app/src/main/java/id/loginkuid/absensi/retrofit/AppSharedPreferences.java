package id.loginkuid.absensi.retrofit;


import android.content.Context;
import android.content.SharedPreferences;

public class AppSharedPreferences {

    /*
    Shared preference names
     */
    private static final String APP_CONTEXT = "app-context";
    private static final String USER_ID = "user_id";
    private static final String USER_EMAIL = "user-email";
    private static final String USER_PHONE_NUMBER = "user-phone-number";
    private static final String TARGET_ANGGARAN_DARAT = "target-anggaran-darat";
    private static final String TARGET_ANGGARAN_LAUT = "target-anggaran-laut";
    private static final String TARGET_ANGGARAN_UDARA = "target-anggaran-udara";

    private static final String REALISASI_ANGGARAN_DARAT = "realisasi-anggaran-darat";
    private static final String REALISASI_ANGGARAN_LAUT = "realisasi-anggaran-laut";
    private static final String REALISASI_ANGGARAN_UDARA = "realisasi-anggaran-udara";

    private static final String USER_DISPLAY_NAME = "user-display-name";
    private static final String USER_PICTURE_URL = "user-picture-url";
    private static final String USER_ROLE = "user-role";
    private static final String LAST_LOCATION_LATITUDE = "last-location-latitude";
    private static final String LAST_LOCATION_LONGITUDE = "last-location-longitude";
    private static final String LAST_LOCATION_ADDRESS = "last-location-address";

    public static final String LOGGED_IN_PREF = "logged_in_status";

    private static final String SHARED_PREF_NAME = "simplifiedcodingsharedprefretrofit";

    private static final String KEY_USER_ID = "keyuserid";
    private static final String KEY_USER_NAME = "keyusername";
    private static final String KEY_USER_EMAIL = "keyuseremail";
    private static final String KEY_USER_GENDER = "keyusergender";

    /*
    Default coordinate is Medan
     */
    public static final String DEFAULT_LATITUDE = "3.58333";
    public static final String DEFAULT_LONGITUDE = "98.66667";
    public static final String DEFAULT_ADDRESS = "Kota Medan";

    /*
    Public constant
     */
    public static final String NO_URL = "no-url";

    private static Context mCtx;
    private static AppSharedPreferences mInstance;

    private AppSharedPreferences(Context context) {
        mCtx = context;
    }

    public static synchronized AppSharedPreferences getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AppSharedPreferences(context);
        }
        return mInstance;
    }

    public static void logOutCurrentUser(Context context) {
        storeUserInformation(context, 0, "", "");
    }

    /**
     * Store current user's display name, email and phone number
     *
     * @param context application context

     * @param displayName current user's display name
     * @param email current user's email
     * @param role current user's phone number

     */
    public static void storeUserInformation(Context context,
                                            int uid,
                                            String displayName,
                                            String email

//            ,
//                                            int role
    ) {

        storeUserId(context, uid);
        storeUserDisplayName(context, displayName);
        storeUserEmail(context, email);
//        storeUserRole(context, role);
    }

    public boolean userLogin(User user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_USER_ID, user.getId());
        editor.putString(KEY_USER_NAME, user.getNama());
        editor.putString(KEY_USER_EMAIL, user.getEmail());
        editor.putInt(KEY_USER_GENDER, user.getId());
        editor.apply();
        return true;
    }

    public static void setLoggedIn(Context context, boolean loggedIn) {
        SharedPreferences.Editor editor = context.getSharedPreferences(APP_CONTEXT, 0).edit();
        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.apply();
    }

    public static boolean getLoggedStatus(Context context) {
        return context.getSharedPreferences(APP_CONTEXT, 0).getBoolean(LOGGED_IN_PREF, false);
    }

    /**
     * Store current user's uid to shared-preference
     *
     * @param context application context
     * @param uid current user's uid
     */
    public static void storeUserId(Context context, int uid) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putInt(USER_ID, uid);
        editor.apply();
    }

    /**
     * Fetch current user's uid from shared-preference
     *
     * @param context application context
     * @return current user's uid
     */
    public static int getUserId(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getInt(USER_ID, -1);
    }

    /**
     * Get current user's email from shared-preference
     *
     * @param context application context
     * @return current user's email
     */
    public static int getUserRole(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getInt(USER_ROLE, -1);
    }


    public static void storeUserRole(Context context, int role) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putInt(USER_ROLE, role);
        editor.apply();
    }

    /**
     * Store current user's display name to shared-preference
     *
     * @param context application context
     * @param displayName current user's display name
     */
    public static void storeUserDisplayName(Context context, String displayName) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(USER_DISPLAY_NAME, displayName);
        editor.apply();
    }

    public static void storeUserTargetAnggaranDarat(Context context, String targetAnggaranDarat) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(TARGET_ANGGARAN_DARAT, targetAnggaranDarat);
        editor.apply();
    }

    public static void storeUserTargetAnggaranLaut(Context context, String targetAnggaranLaut) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(TARGET_ANGGARAN_LAUT, targetAnggaranLaut);
        editor.apply();
    }

    public static void storeUserTargetAnggaranUdara(Context context, String targetAnggaranUdara) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(TARGET_ANGGARAN_UDARA, targetAnggaranUdara);
        editor.apply();
    }

    /**
     * Get current user's display name from shared-preference
     *
     * @param context application context
     * @return current user's display name
     */
    public static String getUserDisplayName(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getString(USER_DISPLAY_NAME, "");
    }

    /**
     * Store current user's email to shared-preference
     *
     * @param context application context
     * @param email current user's email
     */
    public static void storeUserEmail(Context context, String email) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(USER_EMAIL, email);
        editor.apply();
    }



    /**
     * Store current user's phone number to shared-preference
     *
     * @param context application context
     * @param phoneNumber current user's phone number
     */
    public static void storeUserPhoneNumber(Context context, String phoneNumber) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(USER_PHONE_NUMBER, phoneNumber);
        editor.apply();
    }

    /**
     * Get current user's phone number from shared-preference
     *
     * @param context application context
     * @return current user's phone number
     */
    public static String getUserPhoneNumber(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getString(USER_PHONE_NUMBER, "");
    }

    /**
     * Store current user's picture url to shared-preference
     *
     * @param context application context
     * @param pictureUrl current user's phone number
     */
    public static void storeUserPictureUrl(Context context, String pictureUrl) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(USER_PICTURE_URL, pictureUrl);
        editor.apply();
    }


    public static String getUserEmail(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getString(USER_EMAIL, "");
    }



    /**
     * Get current user's picture url from shared-preference
     *
     * @param context application context
     * @return current user's picture url
     */
    public static String getUserPictureUrl(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getString(USER_PICTURE_URL, "");
    }

    /**
     * Store current user's last latitude location to shared-preference
     *
     * @param context application context
     * @param latitude current's user last latitude location
     */
    public static void storeUserLastLatitudeLocation(Context context, String latitude) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(LAST_LOCATION_LATITUDE, latitude);
        editor.apply();
    }

    /**
     * Get current user's last latitude location from shared-preference
     *
     * @param context application context
     * @return current user's last latitude location
     */
    public static String getUserLastLatitudeLocation(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getString(LAST_LOCATION_LATITUDE, DEFAULT_LATITUDE);
    }

    /**
     * Store current user's last longitude location to shared-preference
     *
     * @param context application context
     * @param longitude current user's last longitude location
     */
    public static void storeUserLastLongitudeLocation(Context context, String longitude) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(LAST_LOCATION_LONGITUDE, longitude);
        editor.apply();
    }

    /**
     * Get current user's last longitude location from shared-preference
     *
     * @param context application context
     * @return current user's last longitude location
     */
    public static String getUserLastLongitudeLocation(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getString(LAST_LOCATION_LONGITUDE, DEFAULT_LONGITUDE);
    }

    /**
     * Store current user's last address to shared-preference
     *
     * @param context application context
     * @param address current user's last address
     */
    public static void storeUserLastAddress(Context context, String address) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        SharedPreferences.Editor editor = appContext.edit();
        editor.putString(LAST_LOCATION_ADDRESS, address);
        editor.apply();
    }

    /**
     * Get current user's last address from shared-preference
     *
     * @param context application context
     * @return current user's last address
     */
    public static String getUserAddress(Context context) {
        SharedPreferences appContext = context.getSharedPreferences(APP_CONTEXT, 0);
        return appContext.getString(LAST_LOCATION_ADDRESS, DEFAULT_ADDRESS);
    }

}
