package id.loginkuid.absensi.dashboard.signaturepad;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import id.loginkuid.absensi.R;

import static java.lang.String.format;

public class OpenAndSavePDFActivity2 extends AppCompatActivity implements OnPageChangeListener {

    private static final String TAG = "open-and-save";
    Button openPDF, savePDF,signPDF;
    PDFView pdfView;

    String pdfFileName;
    File file;
    private int pageNumber = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_save_pdf2);

        openPDF = findViewById(R.id.openPDF);
        savePDF = findViewById(R.id.savePDF);
        signPDF = findViewById(R.id.signPDF);

        pdfView = findViewById(R.id.pdfView);

        isExternalStorageWritable();
        isExternalStorageReadable();




        openPDF.setOnClickListener(view ->{

            Pickpdfstorage();

        });


    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    private void Pickpdfstorage() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        startActivityForResult(intent, 10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 10:
                // Get the Uri of the selected file
                if (resultCode == RESULT_OK) {

                    if (null != data.getData()) {

                        Uri uri = data.getData();


                        if (uri.getScheme().equals("content")) {

                            file = new File(getCacheDir(), data.getData().getLastPathSegment());

                            try {
                                InputStream iStream = getContentResolver().openInputStream(uri);
                                FileOutputStream output = null;
                                output = new FileOutputStream(file);
                                final byte[] buffer = new byte[1024];
                                int size;
                                while ((size = iStream.read(buffer)) != -1) {
                                    output.write(buffer, 0, size);
                                }
                                iStream.close();
                                output.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else
                            file = new File(uri.getPath());
                        Log.d(TAG, "Filepath : " +file);




//
                        pdfFileName = file.getName();

                        // make something with the name




                        try {
//                            OpenPdfActivity(file.getAbsolutePath());
                            OpenPdfActivity(file, uri);
//                            createPdf(file.getAbsolutePath());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
    }

    private void OpenPdfActivity (File Filepath, Uri uri) {


        Log.d(TAG, "Filepath : " +Filepath);
        pdfView.fromFile(Filepath)
                .pages(0, 2, 1, 3, 3, 3)
                .defaultPage(1)
                .showMinimap(false)
                .enableSwipe(true)
                .load();

    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(format("%s %s / %s", pdfFileName, page, pageCount));
    }
}
