package id.loginkuid.absensi.dashboard.signaturepad.DrawOnCanvas;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.loginkuid.absensi.R;

public class OpenAndDrawSavePDF extends AppCompatActivity {

    private static final String TAG = "open-save-draw";

    String path = "";

    Button openPDF, savePDF;

    File file;
    String pdfFileName;
    OutputStream outputStream;

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_open_save_draw_pdf);


        openPDF = findViewById(R.id.openPDF);
        savePDF = findViewById(R.id.savePDF);


        openPDF.setOnClickListener(view -> {

            Pickpdfstorage();

        });







//        RelativeLayout parent = (RelativeLayout) findViewById(R.id.signImageParent);
//        MyDrawView myDrawView = new MyDrawView(this);
//        parent.addView(myDrawView);
//
//
//        parent.setDrawingCacheEnabled(true);
//        Bitmap b = parent.getDrawingCache();
//
//
//
//
//        File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_DOCUMENTS), "pdfFile");
//
//        if (!pdfFolder.exists()) {
//            pdfFolder.mkdir();
//            Log.i(TAG, "Pdf Directory created");
//            Log.i(TAG, "Pdf Directory created" +pdfFolder.mkdir());
//        }
//
//        //Create time stamp
//        Date date = new Date() ;
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
//
//
//        path = pdfFolder.getAbsolutePath();
//
//        File myFile = new File(pdfFolder + timeStamp  + ".pdf");
//        Log.d(TAG, "myfile sebelum setsigner: " +myFile);
//
////            File myFile = new File(pdfFolder + timeStamp + ".pdf");
//
////            outputStream = null;
//
//
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(myFile.getAbsolutePath());
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
////        b.compress(Bitmap.CompressFormat.PNG, 95, fos);


    }



    private void Pickpdfstorage() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        startActivityForResult(intent, 10);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 10:
                // Get the Uri of the selected file
                if (resultCode == RESULT_OK) {

                    if (null != data.getData()) {

                        Uri uri = data.getData();


                        if (uri.getScheme().equals("content")) {

                            file = new File(getCacheDir(), data.getData().getLastPathSegment());

                            try {
                                InputStream iStream = getContentResolver().openInputStream(uri);
                                FileOutputStream output = null;
                                output = new FileOutputStream(file);
                                final byte[] buffer = new byte[1024];
                                int size;
                                while ((size = iStream.read(buffer)) != -1) {
                                    output.write(buffer, 0, size);
                                }
                                iStream.close();
                                output.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else
                            file = new File(uri.getPath());



//
                        pdfFileName = file.getName();
                        // make something with the name




                        try {
//                            OpenPdfActivity(file.getAbsolutePath());
                            drawRectanglePDF(file, uri);
//                            createPdf(file.getAbsolutePath());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
    }

    public void drawRectanglePDF (File src, Uri dest)
            throws IOException, DocumentException {

        File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), "pdfPindahRect");

        if (!pdfFolder.exists()) {
            pdfFolder.mkdir();
            Log.i(TAG, "Pdf Directory created");
            Log.i(TAG, "Pdf Directory created" +pdfFolder.mkdir());
        }

        //Create time stamp
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);


        path = pdfFolder.getAbsolutePath();

        File myFile = new File(pdfFolder + timeStamp  + ".pdf");
        Log.d(TAG, "myfile sebelum setsigner: " +myFile);



        try {
            outputStream = new FileOutputStream(myFile);
            Log.d(TAG, "outputStream :" +outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Creating a reader
        PdfReader reader = new PdfReader(String.valueOf(src));
        // step 1
        Rectangle pageSize = reader.getPageSize(reader.getNumberOfPages());
        Rectangle toMove = new Rectangle(100, 500, 200, 600);
        Document document = new Document(pageSize);
        // step 2
        PdfWriter writer
                = PdfWriter.getInstance(document, new FileOutputStream(myFile));
        // step 3
        document.open();
        // step 4
        PdfImportedPage page = writer.getImportedPage(reader, 1);
        PdfContentByte cb = writer.getDirectContent();
        PdfTemplate template1 = cb.createTemplate(pageSize.getWidth(), pageSize.getHeight());
        template1.rectangle(0, 0, pageSize.getWidth(), pageSize.getHeight());
        template1.rectangle(toMove.getLeft(), toMove.getBottom(),
                toMove.getWidth(), toMove.getHeight());
        template1.eoClip();
        template1.newPath();
        template1.addTemplate(page, 0, 0);
        PdfTemplate template2 = cb.createTemplate(pageSize.getWidth(), pageSize.getHeight());
        template2.rectangle(toMove.getLeft(), toMove.getBottom(),
                toMove.getWidth(), toMove.getHeight());
        template2.clip();
        template2.newPath();
        template2.addTemplate(page, 0, 0);
        cb.addTemplate(template1, 0, 0);
        cb.addTemplate(template2, -20, -2);
        // step 4
        document.close();
        reader.close();

    }

}
