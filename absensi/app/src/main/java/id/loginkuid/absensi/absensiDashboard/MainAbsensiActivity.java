package id.loginkuid.absensi.absensiDashboard;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.loginkuid.absensi.R;
import id.loginkuid.absensi.authentication.SelfieCameraActivity3;

public class MainAbsensiActivity extends AppCompatActivity {

    private static final String TAG = "main_activity";
    Button takeSelfibtn, sendLocationtbtn;


    CardView sendAllCV;

    String strID;


    String path, path2;

    ImageView selfieImage;

    Uri uri;

    ProgressDialog progressDialog;

    String finalResult ;

    TextView alamat,tanggalTV;

    HashMap<String,String> hashMap = new HashMap<>();

    HttpParse httpParse = new HttpParse();
    String HttpURLLocation = "http://www.yukmarry.com/absensi/SendLocationBener.php";

    public static final String UPLOAD_URL = "http://www.yukmarry.com/absensi/upload.php";
    public static final String UPLOAD_KEY = "Image";

    String P_Number_Holder, Latitude_Holder , Longitude_Holder, Address_Holder, DateTime_Holder, Image_Holder,
            city_Address_Holder, state_Address_Holder, country_Address_Holder, postalCode_Address_Holder,knownName_Address_Holder;


    Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_absensi);

        sendAllCV = findViewById(R.id.sendAllCV);


        takeSelfibtn = findViewById(R.id.takeSelfieBtn);
        selfieImage =findViewById(R.id.previewImage);
        sendLocationtbtn =findViewById(R.id.sendLocationtbtn);

        alamat= findViewById(R.id.alamat);
        tanggalTV= findViewById(R.id.tanggalTV);





        sendAllCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //check if locatieon and photo is there not null (validation)

                //method to pass String above to send location to php SERVER

                Log.d(TAG, "Image_Holder : " +Image_Holder);

                UserSendLocationtoServerFunction(Latitude_Holder , Longitude_Holder, Address_Holder, DateTime_Holder);


                uploadImage();

            }
        });


        sendLocationtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShowUserLoginCurrentLocation();


            }
        });


        takeSelfibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent (MainAbsensiActivity.this, SelfieCameraActivity3.class);

                startActivityForResult(intent, SelfieCameraActivity3.REQUEST_CODE_SELFIE);

            }
        });





    }

    private void ShowUserLoginCurrentLocation() {
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) MainAbsensiActivity.this.getSystemService(Context.LOCATION_SERVICE);
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {


                // Called when a new location is found by the network location provider.
                String lat = Double.toString(location.getLatitude());
                String lon = Double.toString(location.getLongitude());

                //String LatitudeHolder = txtLoc.getText().toString();
                //String LongitudeHolder =
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(MainAbsensiActivity.this, Locale.getDefault());

                double latitude= Double.parseDouble(lat);
                double longitude= Double.parseDouble(lon);

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                    //txtAddress.setText("Alamat dari latitude longitude :"  + address + " ," + city + " ," + country);

                    //Date currentTime = Calendar.getInstance().getTime();
                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

                    //tvDateTime.setText("Waktu Login : " + currentDateTimeString);
//                    P_Number_Holder = PhoneNumber.getText().toString();
                    Latitude_Holder = lat;
                    Longitude_Holder = lon;
                    Address_Holder = address;
                    //blum di pass
                    city_Address_Holder = city;
                    state_Address_Holder = state;
                    country_Address_Holder = country;
                    postalCode_Address_Holder = postalCode;
                    knownName_Address_Holder = knownName;

                    Log.d(TAG, "Longitude_Holder 1 : " + Latitude_Holder );
                    Log.d(TAG, "Longitude_Holder 2 : " + Longitude_Holder );
                    Log.d(TAG, "Longitude_Holder 2 : " + Address_Holder );
                    Log.d(TAG, "Longitude_Holder 3 : " + city_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 4 : " + state_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 5 : " +country_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 6 : " + postalCode_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 7 : " + knownName_Address_Holder );


                    DateTime_Holder = currentDateTimeString;

                    Log.d(TAG, "Longitude_Holder 7 : " + DateTime_Holder );

                    alamat.setText(Address_Holder);
                    tanggalTV.setText(DateTime_Holder);




//                    UserSendLocationtoServerFunction(Latitude_Holder , Longitude_Holder, Address_Holder, DateTime_Holder);




                } catch (IOException e) {
                    // print ke log jika Error
                    e.printStackTrace();
                }


            }

            public void onStatusChanged(String provider, int status, Bundle extras) { }
            public void onProviderEnabled(String provider) { }
            public void onProviderDisabled(String provider) { }
        };

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(MainAbsensiActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(MainAbsensiActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

    }



    public void UserSendLocationtoServerFunction(final String Latitude, final String Longitude, final String Address,final String DateTime){

        class UserSendLocationtoServerClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(MainAbsensiActivity.this,"Loading Data",null,true,true);

            }


            @Override
            protected void onPostExecute(String httpResponseMsg) {

                super.onPostExecute(httpResponseMsg);

                progressDialog.dismiss();

//                alamat.setText(Address_Holder);
//                tanggalTV.setText(DateTime_Holder);
//
//                alamat.setText(Address);
//                tanggalTV.setText(DateTime);

                finish();

                Intent intent = new Intent(MainAbsensiActivity.this, SentAllActivity.class);
/*
                    intent.putExtra(latitude,Latitude);
                    intent.putExtra(longitude,Longitude);
                    intent.putExtra(Addressextra,Address);
                    intent.putExtra(datetimeextra,DateTime);
*/
                startActivity(intent);
                finish();



                Toast.makeText(MainAbsensiActivity.this,httpResponseMsg.toString(), Toast.LENGTH_LONG).show();


//                finish();

            }


            @Override
            protected String doInBackground(String... params) {

//                hashMap.put("P_number",params[0]);

//                strID = hashMap.get("id");
//                Log.d(TAG, "strID : " +strID);

                hashMap.put("Latitude",params[0]);
                Log.d(TAG, "params[0] : " +params[0]);

                hashMap.put("Longitude",params[1]);
                Log.d(TAG, "params[1] : " +params[1]);

                hashMap.put("Address",params[2]);
                Log.d(TAG, "params[2] : " +params[2]);

                hashMap.put("DateTime",params[3]);
                Log.d(TAG, "params[3] : " +params[3]);

//                hashMap.put("Image", params[4]);
//                Log.d(TAG, "params[4] : " +params[4]);

                finalResult = httpParse.postRequest(hashMap, HttpURLLocation);

                Log.d(TAG, "HttpURLLocation : " +HttpURLLocation);

                return finalResult;


            }


        }

        UserSendLocationtoServerClass userSendLocationtoServerClass = new UserSendLocationtoServerClass();

//        userSendLocationtoServerClass.execute(Latitude,Longitude,Address,DateTime, Image);
        userSendLocationtoServerClass.execute(Latitude,Longitude,Address,DateTime);


    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage(){
        class UploadImage extends AsyncTask<Bitmap,Void,String>{

            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
//                loading = ProgressDialog.show(MainAbsensiActivity.this, "Uploading...", null,true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
//                loading.dismiss();
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Bitmap... params) {

                Bitmap bitmap = params[0];
                //upload image blum sesuai ID

                Log.d(TAG, "bitmap onbg : " +bitmap);

                String uploadImage = getStringImage(bitmap);

                HashMap<String,String> data = new HashMap<>();


//                data.put("id", strID);
//                Log.d(TAG, "strID : " +strID);
                data.put(UPLOAD_KEY, uploadImage);
                String result = rh.sendPostRequest(UPLOAD_URL,data);

                return result;
            }
        }

        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SelfieCameraActivity3.REQUEST_CODE_SELFIE && resultCode == SelfieCameraActivity3.RESULT_CODE_SELFIE){
            path = SelfieCameraActivity3.getImagePath(data);

            uri = data.getData();
            Log.d(TAG, "uri : " +uri);
            //send uri data to PHP server

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                Image_Holder = getStringImage(bitmap);
                Log.d(TAG, "Image_Holder : " +Image_Holder);

            } catch (IOException e) {
                e.printStackTrace();
            }


            Log.d(TAG, "PATH =>" + path);
            if (!TextUtils.isEmpty(path)) {
                String result = data.getStringExtra("IMAGE_PATH_SELFIE");
                Log.d(TAG, "PATH =>" + result);

//                toSelfie.setTextColor(getResources().getColor(R.color.BLUE));
//                toSelfie.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ico_check, 0, 0, 0);
//                selfieImage.setVisibility(View.VISIBLE);
//                selfieImage.setImageBitmap(BitmapFactory.decodeFile(result));

                takeSelfibtn.setTextColor(getResources().getColor(R.color.BLUE));
                takeSelfibtn.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ico_check, 0, 0, 0);

                selfieImage.setVisibility(View.VISIBLE);
                selfieImage.setImageBitmap(BitmapFactory.decodeFile(result));
                selfieImage.setRotation(180);

                bitmap = BitmapFactory.decodeFile(result);
                Log.d(TAG, "PATH =>" + bitmap);
            }


        }



    }
}