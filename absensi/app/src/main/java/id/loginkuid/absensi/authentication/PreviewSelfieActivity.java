package id.loginkuid.absensi.authentication;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import id.loginkuid.absensi.R;

public class PreviewSelfieActivity extends AppCompatActivity implements View.OnClickListener {


    public static final int REQUEST_CODE_SELFIE = 1 ;
    public static final int RESULT_CODE_SELFIE = 2;

    private static final String TAG = "preview-selfie";

    ImageView previewImage;

    String path;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_selfie);

        previewImage =findViewById(R.id.previewImage);


        Intent intent = getIntent();
        path = intent.getStringExtra("IMAGE_PATH_SELFIE");
        Log.d(TAG, "PATH =>" +path);

        previewImage.setImageBitmap(BitmapFactory.decodeFile(path));

        findViewById(R.id.iv_camera_result_ok).setOnClickListener( this);
        findViewById(R.id.iv_camera_result_cancel).setOnClickListener(this);


    }

    public static String getImagePath(Intent data) {
        if (data != null) {
            return data.getStringExtra("IMAGE_PATH_SELFIE2");
        }
        return "";
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_camera_result_ok) {
//             startSavingPhoto(bitmap);

           Intent intent = new Intent(this, RegistrasiActivity.class);
           intent.putExtra("IMAGE_PATH_SELFIE2",path );
//            setResult(REQUEST_CODE_SELFIE,intent);
           startActivity(intent);
           finish();

        } else if (id == R.id.iv_camera_result_cancel) {
            Intent intent = new Intent(this, SelfieCameraActivity3.class);
            startActivity(intent);
            finish();

        }
    }

}
