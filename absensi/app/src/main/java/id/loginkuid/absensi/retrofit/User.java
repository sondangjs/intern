package id.loginkuid.absensi.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("id_role")
    @Expose
    private int idRole;

    /**
     * No args constructor for use in serialization
     *
     */
    public User() {
    }

    public User( String nama, String password) {
//        this.nama = name;
        this.nama = nama;
        this.password = password;
//        this.idRole = idRole;
    }

    public User(String name, String email, String password, int idRole) {
        this.nama = name;
        this.email = email;
        this.password = password;
//        this.idRole = idRole;
    }

    public User(int id, String name, String email, int idRole) {
        this.id = id;
        this.nama = name;
        this.email = email;
        this.idRole = idRole;
    }

    public User(String email, String password, int idRole) {

        this.email = email;
        this.password = password;
        this.idRole = idRole;
    }

    /**
     *
     * @param id
     * @param email
     * @param idRole
     * @param nama
     * @param password
     */
    public User(int id, String nama, String email, String password, int idRole) {
        super();
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.password = password;
        this.idRole = idRole;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

}
