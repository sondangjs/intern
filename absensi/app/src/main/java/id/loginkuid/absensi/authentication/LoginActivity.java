package id.loginkuid.absensi.authentication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import id.loginkuid.absensi.MainActivity;
import id.loginkuid.absensi.R;
import id.loginkuid.absensi.absensiDashboard.MainAbsensiActivity;
import id.loginkuid.absensi.authentication.utils.SharedPrefManager;
import id.loginkuid.absensi.retrofit.APIInterface;
import id.loginkuid.absensi.retrofit.UtilsApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import id.loginkuid.loginku.dashboard.signaturepad.PdfLoad;

public class LoginActivity extends AppCompatActivity {
//
//    @BindView(R.id.etEmail) EditText etEmail;
//    @BindView(R.id.etPasssword) EditText etPassword;
//    @BindView(R.id.toLogin) Button btnLogin;
//    @BindView(R.id.btnRegister) Button btnRegister;
    ProgressDialog loading;

    Context mContext;
    APIInterface mApiService;

    SharedPrefManager sharedPrefManager;

    private static final String TAG = "login-activity";
    Button toLogin, toRegis;
    TextView toForgot;

    EditText usernameET, passwordET;
    String username, password;

    public ProgressDialog mProgressDialog;


    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading ...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //  getSupportActionBar().hide();

        toForgot = findViewById(R.id.toForgot);
        toLogin = findViewById(R.id.toLogin);
        toRegis = findViewById(R.id.toRegis);

        usernameET = findViewById(R.id.usernameET);
        passwordET = findViewById(R.id.editText3);

        username = usernameET.getText().toString();
        password = passwordET.getText().toString();

        ButterKnife.bind(this);
        mContext = this;
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper
        sharedPrefManager = new SharedPrefManager(this);


        toRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, RegistrasiActivity.class));
            }
        });

        toLogin.setOnClickListener(new View.OnClickListener() {
            @Override
//            public void onClick(View v) {
//                loading = ProgressDialog.show(mContext, null)
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainAbsensiActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
//                loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
//                requestLogin();
//            }
//        });
//        if (sharedPrefManager.getSPSudahLogin()){
//            startActivity(new Intent(LoginActivity.this, MainActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//            finish();
//        }
//    }
//yangku pahamii..



private void requestLogin(){
    mApiService.loginRequest(usernameET.getText().toString(),passwordET.getText().toString())
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()){
                        loading.dismiss();
                        try {
                            JSONObject jsonRESULTS = new JSONObject(response.body().string());
                            if (jsonRESULTS.getString("error").equals("false")){
                                // Jika login berhasil maka data nama yang ada di response API
                                // akan diparsing ke activity selanjutnya.
                                Toast.makeText(mContext, "BERHASIL LOGIN", Toast.LENGTH_SHORT).show();
                                String nama = jsonRESULTS.getJSONObject("user").getString("nama");
                                sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA, nama);
                                // Shared Pref ini berfungsi untuk menjadi trigger session login
                                sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);
                                startActivity(new Intent(mContext, MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                finish();
                            } else {
                                // Jika login gagal
                                String error_message = jsonRESULTS.getString("error_msg");
                                Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("debug", "onFailure: ERROR > " + t.toString());
                    loading.dismiss();
                }
            });
}
}
