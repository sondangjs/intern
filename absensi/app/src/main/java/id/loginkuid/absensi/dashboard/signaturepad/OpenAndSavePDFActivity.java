package id.loginkuid.absensi.dashboard.signaturepad;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.gcacace.signaturepad.views.SignaturePad;
//import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.xmp.XMPException;
import com.itextpdf.signatures.DigestAlgorithms;
import com.itextpdf.signatures.ICrlClient;
import com.itextpdf.signatures.IOcspClient;
//import com.itextpdf.signatures.ITSAClient;
import com.itextpdf.signatures.ITSAClient;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
//import com.itextpdf.text.pdf.PdfReader;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
//import com.itextpdf.signatures.PdfSignatureAppearance;


//import com.itextpdf.kernel.pdf.PdfWriter;
//import com.shockwave.pdfium.PdfDocument;


import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.CertificateUtil;
import com.itextpdf.text.pdf.security.ExternalDigest;
//import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OCSPVerifier;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;
import com.shockwave.pdfium.PdfDocument;

import org.spongycastle.asn1.esf.SignaturePolicyIdentifier;
//import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import id.loginkuid.absensi.R;
//import id.loginkuid.loginku.dashboard.signaturepad.Custom.CustomMakeSignature;
import id.loginkuid.absensi.dashboard.signaturepad.Custom.CustomMakeSignature;
import id.loginkuid.absensi.dashboard.signaturepad.Custom.CustomPrivateKeySignature;
import id.loginkuid.absensi.dashboard.signaturepad.Custom.FilePath;

//import static id.loginkuid.loginku.dashboard.signaturepad.Custom.CustomMakeSignature.signDetached;

public class OpenAndSavePDFActivity extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener {

    private static final String TAG = "opensave-activity";
    public static final int PERMISSION_CODE = 42042;
    private final static int REQUEST_CODE = 42;


    String uriPDF;

    PDFView pdfView;
    TextView statusTextView;

    PdfReader reader;
    PdfStamper stamper;

    BaseFont bf;

    File file;

    Integer pageNumber = 0;

    String pdfFileName;

    Button savePDF, giveInk,openLagiPDF;

    String downloadFileName = "";

    PdfImportedPage page;

    String alias;
    String path = "";

    File photo,svgFile;

    EditText etLocation, etReason;

    OutputStream outputStream, outputStream2;

    private SignaturePad mSignaturePad;

    private Button mClearButton;
    private Button mSaveButton;
    private Button openPDFsaved;

    Bitmap newBitmap;

    public ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading ... PDF in Process");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    String Reason, Location;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_open_save_pdf);


        mClearButton = (Button) findViewById(R.id.clear_button);
        mSaveButton = (Button) findViewById(R.id.save_button);
        openPDFsaved = (Button) findViewById(R.id.openPDFsaved);
        statusTextView = findViewById(R.id.statusTextView);



        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                Toast.makeText(OpenAndSavePDFActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });
        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

//                showProgressDialog();
                Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
//                saveImage(signatureBitmap);

                if (saveImage(signatureBitmap)) {
                    Toast.makeText(OpenAndSavePDFActivity.this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(OpenAndSavePDFActivity.this, "Unable to store the signature", Toast.LENGTH_SHORT).show();
                }
//                if (addSvgSignatureToGallery(mSignaturePad.getSignatureSvg())) {
//                    Toast.makeText(OpenAndSavePDFActivity.this, "SVG Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(OpenAndSavePDFActivity.this, "Unable to store the SVG signature", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        isExternalStorageWritable();
        isExternalStorageReadable();

        Button btnopenPDF = findViewById(R.id.openPDF);
        savePDF = findViewById(R.id.savePDF);
//        giveInk = findViewById(R.id.giveInk);
        openLagiPDF = findViewById(R.id.openLagiPDF);
        etLocation = findViewById(R.id.etLocation);
        etReason = findViewById(R.id.etReason);

        Reason = etReason.getText().toString();
        Location = etLocation.getText().toString();

        Log.d(TAG, "Reason :" +Location);
        Log.d(TAG, "Reason :" +Reason);



        pdfView = (PDFView) findViewById(R.id.pdfView);



        btnopenPDF.setOnClickListener(view -> {
            Pickpdfstorage();


            statusTextView.setText("");

            openPDFsaved.setVisibility(View.GONE);

            openPDFsaved.setEnabled(false);
        });



    }

    public boolean saveImage(Bitmap finalBitmap) {
        boolean result = false;

        photo = new File(getAlbumStorageDir("TandatanganSignaturePad"), String.format("Signature_%d.png", System.currentTimeMillis()));
        try {
            saveBitmapToJPG(finalBitmap, photo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        scanMediaFile(photo);
        result = true;
        return result;

    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        OpenAndSavePDFActivity.this.sendBroadcast(mediaScanIntent);
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {

        newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.TRANSPARENT);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);


//        hideProgressDialog();

        stream.close();


    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }



    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    // Method for opening a pdf file
    private void viewPdf(String file) {

        File pdfFile = new File(Environment.getExternalStorageDirectory()  + "/" + file);
        Uri path = Uri.fromFile(pdfFile);

        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(OpenAndSavePDFActivity.this, "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }

    private void Pickpdfstorage() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        startActivityForResult(intent, 10);
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"resultCode :" +resultCode);
        switch (requestCode) {

            case 10:
                // Get the Uri of the selected file
                if (resultCode == RESULT_OK) {

                    if (null != data.getData()) {

                        Uri uri = data.getData();
                        Log.d(TAG, "file : uri"+uri);

                        String selectedFilePath = FilePath.getPath(this, uri);
                        file = new File(selectedFilePath);

                        Log.d(TAG, "file : 2"+file);

//                        pdfFileName = file.getName();
                        pdfFileName = getFileName(uri);

//                        file = new File(uri.getPath());


//                        if (uri.getScheme().equals("content")) {
//
//                            file = new File(getCacheDir(), data.getData().getLastPathSegment());
//                            Log.d(TAG, "file : 1"+file);
//
//                            try {
//                                InputStream iStream = getContentResolver().openInputStream(uri);
//                                FileOutputStream output = null;
//                                output = new FileOutputStream(file);
//                                final byte[] buffer = new byte[1024];
//                                int size;
//                                while ((size = iStream.read(buffer)) != -1) {
//                                    output.write(buffer, 0, size);
//                                }
//                                iStream.close();
//                                output.close();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        } else
//                            file = new File(uri.getPath());
//                        Log.d(TAG, "file : 2"+file);



//

                        // make something with the name




                        try {
//                            OpenPdfActivity(file.getAbsolutePath());
                            OpenPdfActivity(file, uri);
//                            createPdf(file.getAbsolutePath());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getLastPathSegment();
        }
        return result;
    }




    private void OpenPdfActivity (File Filepath, Uri uri) {

        pdfView.fromFile(Filepath)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
//                .onPageScroll(onPageScrollListener)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();


        savePDF.setOnClickListener(view ->{


            Log.d(TAG, "Reason :" +Reason);
            Log.d(TAG, "Reason :" +Location);


            //berhasil create file PDF from opened pdf



            File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS), "tandatangan");

            if (!pdfFolder.exists()) {
                pdfFolder.mkdir();
                Log.i(TAG, "Pdf Directory created");
                Log.i(TAG, "Pdf Directory created" +pdfFolder.mkdir());
            }

            //Create time stamp
            Date date = new Date() ;
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);


            path = pdfFolder.getAbsolutePath();

            File myFile = new File(pdfFolder + timeStamp  + ".pdf");
            Log.d(TAG, "myfile sebelum setsigner: " +myFile);

//            File myFile = new File(pdfFolder + timeStamp + ".pdf");

//            outputStream = null;


            try {
                outputStream = new FileOutputStream(myFile);
                Log.d(TAG, "outputStream :" +outputStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }



            //BERHASIL CREATE FILE AND FOLDER


//             Create output PDF
//            Document document = new Document(PageSize.A4);
//            PdfWriter writer = null;
//            try {
//                writer = PdfWriter.getInstance(document, outputStream);
//            } catch (DocumentException e) {
//                e.printStackTrace();
//            }
//            document.open();

            Log.d(TAG, "outputStream : "+outputStream);

            try {
                try {
                    testSignLikeXinDHA(Filepath, outputStream, myFile, uri);
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                } catch (XMPException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
//
//
//            PdfContentByte cb = writer.getDirectContent();
//
//// Load existing PDF
////            PdfReader reader = null;
//            try {
//                reader = new PdfReader(String.valueOf(Filepath));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            PdfImportedPage page = writer.getImportedPage(reader, 1);

// Copy first page of existing PDF into output PDF


//            document.newPage();
//            cb.addTemplate(page, 0, 0);
//            document.newPage();




// Add your new data / text here
// for example...
//            try {
//
//                //berhasil
//                document.add(new Paragraph("my timestamp"));
//            } catch (DocumentException e) {
//                e.printStackTrace();
//            }
//
//            document.close();



//            String imagePath = Filepath.getAbsolutePath();
//
//            String outputPath = imagePath.substring(0, imagePath.lastIndexOf('.')) + ".pdf";
//            downloadFileName = outputPath;
//
//            String pdfPath = Filepath.getAbsolutePath() + "example.pdf";
//
////            savePDF(imagePath, outputPath);
//
//            try {
//                saveAssetFile("example.pdf", pdfPath);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
////            new SavingTask().execute();
//
//
////            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
////            View dialogView = View.inflate(this, R.layout.dialog_title, null);
////
////            final EditText editTitle = (EditText) dialogView.findViewById(R.id.title);
////
////            builder.setView(dialogView);
////            builder.setTitle("Please enter title:");
////            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
////
////                public void onClick(DialogInterface dialog, int whichButton) {
////
////                    String title = editTitle.getText().toString().trim();
//////                    sharedPref.edit()
//////                            .putString("title", title)
//////                            .putString("pathPDF", Environment.getExternalStorageDirectory() +  folder + title + ".pdf")
//////                            .apply();
//////                    createPDF();
////                    savePDF(imagePath, outputPath);
////
////                    InputStream in;
////                    OutputStream out;
////
////                    try {
////
//////                        title = sharedPref.getString("title", null);
//////                        folder = sharedPref.getString("folder", "/Android/data/de.baumann.pdf/");
//////                        String path = sharedPref.getString("pathPDF", Environment.getExternalStorageDirectory() +
//////                                folder + title + ".pdf");
////
////                        in = new FileInputStream(Environment.getExternalStorageDirectory() +  "/" + title + ".pdf");
////                        out = new FileOutputStream(imagePath);
////
////                        byte[] buffer = new byte[1024];
////                        int read;
////                        while ((read = in.read(buffer)) != -1) {
////                            out.write(buffer, 0, read);
////                        }
////                        in.close();
////
////                        // write the output file
////                        out.flush();
////                        out.close();
////                    } catch (Exception e) {
////                        Log.e("tag", e.getMessage());
////                    }
////
//////                    img.setImageResource(R.drawable.image);
////
////                    File pdfFile = new File(Environment.getExternalStorageDirectory() +  "/" + title + ".pdf");
////                    if(pdfFile.exists()){
////                        pdfFile.delete();
////                    }
////
//////                    helper_pdf.pdf_textField(getActivity(), rootView);
//////                    helper_pdf.toolbar(getActivity());
////
////                    Toast.makeText(OpenAndSavePDFActivity.this, "File Created ", Toast.LENGTH_LONG).show();
////
//////                    Snackbar snackbar = Snackbar
//////                            .make(img, getString(R.string.toast_successfully), Snackbar.LENGTH_LONG)
//////                            .setAction(getString(R.string.toast_open), new View.OnClickListener() {
//////                                @Override
//////                                public void onClick(View view) {
//////                                    File file = new File(helper_pdf.actualPath(getActivity()));
////////                                    helper_main.openFile(this, file, "application/pdf", img);
//////                                }
//////                            });
//////                    snackbar.show();
////                }
////            });
////
////            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
////
////                public void onClick(DialogInterface dialog, int whichButton) {
////                    dialog.cancel();
////                }
////            });
////            builder.setNeutralButton("Date", null);
//
//            Log.d(TAG, "imagePath : " +imagePath);
//            Log.d(TAG, "imagePath : " +outputPath);
//            Log.d(TAG, "downloadFileName : " +downloadFileName);

        });

        openLagiPDF.setOnClickListener(view ->{

            viewPdf(outputStream.toString());

        });

//        openLagiPDF.setOnClickListener(view ->{
//
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
//            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intent);
//        });
//
//        pdfView.fromUri(Filepath)

//        pdfView.fromSource(Filepath)

//        pdfView.fromSource(DocumentSource);

//        File file = new File(Environment.getExternalStorageDirectory(),
//                "Report.pdf");
//        Uri path = Uri.fromFile(file);
//        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
//        pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        pdfOpenintent.setDataAndType(path, "application/pdf");
//        try {
//            startActivity(pdfOpenintent);
//
//            uriPDF = file.getAbsolutePath();
//        } catch (ActivityNotFoundException e) {
//
//        }
    }

    public void testSignLikeXinDHA(File filepath, OutputStream outputStream, File myFile
    ,Uri uri) throws GeneralSecurityException, IOException, XMPException
    {



        String path = String.valueOf(getResources().openRawResource(R.raw.rizki_private_key));
        Log.d(TAG, "raw folder : " +path);
//        char[] pass = "99Linux.sh".toCharArray();
        char[] pass = "digisec.asia".toCharArray();
//        char[] pass = "watergate".toCharArray();


        InputStream ins = getResources().openRawResource(R.raw.rizki_private_key);
        File file = createFileFromInputStream(ins);

//        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());



        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        KeyStore ks = KeyStore.getInstance("pkcs12", provider.getName());
//        KeyStore ks = KeyStore.getInstance("pkcs12");
        ks.load(new FileInputStream(file.getAbsolutePath()), pass);
        alias = "rizki mayandi hasibuan";
        Enumeration<String> aliases = ks.aliases();
        while (alias.equals("rizki mayandi hasibuan") == false && aliases.hasMoreElements())
        {
            alias = aliases.nextElement();
        }
//        PrivateKey pk = (PrivateKey) ks.getKey(alias, pass);
        Certificate[] chain = ks.getCertificateChain(alias);

        for (int i = 0; i < chain.length; i++) {
            X509Certificate cert = (X509Certificate)chain[i];
            System.out.println(String.format("[%s] %s", i, cert.getSubjectDN()));
            System.out.println(CertificateUtil.getOCSPURL(cert));
        }


        //double di bawah ada lagi provider
//        BouncyCastleProvider provider = new BouncyCastleProvider();
//        Log.d(TAG, "PROVIDER :" +provider);
//        Security.addProvider(provider);

//        PrivateKey privateKey = null;
//
//        try {
//            privateKey = KeyChain.getPrivateKey(getApplicationContext(), alias);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (KeyChainException e) {
//            e.printStackTrace();
//        }


        //private key di bawah ubah jadi externalsignature pks
//        ExternalSignature pks = new CustomPrivateKeySignature(privateKey, com.itextpdf.text.pdf.security.DigestAlgorithms.SHA256, provider.getName());
        PrivateKey pks = (PrivateKey) ks.getKey(alias, pass);
        Log.d(TAG, "alias : " +alias);
        Log.d(TAG, "pks :" +pks);
        Log.d(TAG, "Reason :" +Reason);
        Log.d(TAG, "Location :" +Location);




        try ( InputStream resource = getClass().getResourceAsStream(filepath.getAbsolutePath()))


        {

            //DigestAlgorithms.SHA1 dibawah diuba lagi jadi digest bountycastle
            sign(filepath, outputStream, myFile, uri,
                    chain, pks, DigestAlgorithms.SHA256, /*"SunJSSE"*/provider.toString(),
                    MakeSignature.CryptoStandard.CADES, etReason.getText().toString(), etLocation.getText().toString(),
                    null, null, null, 0, true);
        }
    }

    private File createFileFromInputStream(InputStream inputStream) {



        File file = new File(Environment.getExternalStorageDirectory(),
                "KeyHolder/KeyFile/");
        if (!file.exists()) {
            if (!file.mkdirs())
                Log.d("KeyHolder", "Folder not created");
            else
                Log.d("KeyHolder", "Folder created");
        } else
            Log.d("KeyHolder", "Folder present");

        path = file.getAbsolutePath();

        try {
            File f = new File(path+"/demo");
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            outputStream.close();
            inputStream.close();

            return f;
        } catch (IOException e) {
            // Logging exception
            e.printStackTrace();
        }

        return null;
    }

    public void sign(File src, OutputStream dest, File myFile,Uri uri,
                     Certificate[] chain, PrivateKey pk,
                     String digestAlgorithm,
                     String provider, MakeSignature.CryptoStandard subfilter, String reason, String location,
                     Collection<ICrlClient> crlList, IOcspClient ocspClient, ITSAClient tsaClient, int estimatedSize,
                     boolean initial)
            throws GeneralSecurityException, IOException, XMPException
    {
        // Creating the reader and the signer





//         Create output PDF
        Document document = new Document();
        PdfWriter writer = null;
        try {
            writer = PdfWriter.getInstance(document, dest); // dest sud be the new source
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.open();
        PdfContentByte cb = writer.getDirectContent();

// Load existing PDF
        PdfReader reader = new PdfReader(src.toString());
//        PdfReader reader = new PdfReader(scr.toString());
//        if(document.getPageNumber() == 1) {





//        int pageCount = document.getPageNumber();
//        Log.d(TAG, "pageCount " +pageCount);
        Log.d(TAG, "pageCount " +reader.getNumberOfPages());

//        if(pageCount == 1) {


        //to make 1st pdf
        for (int i=1; i<= reader.getNumberOfPages(); i++) {

            page = writer.getImportedPage(reader, i);
//            Log.d(TAG, "pageCount " +pageCount);
//            Log.d(TAG, "pageCount " +page);
//        }
//        }
//        if(pageCount > 1) {
//            page = writer.getImportedPage(reader, pageCount);
//            Log.d(TAG, "pageCount " +pageCount);
//            Log.d(TAG, "pageCount " +page);
//        }

// Copy first page of existing PDF into output PDF
            document.newPage();
            cb.addTemplate(page, 0, 0);
//        Log.d(TAG, "pageCount " +page);
//        document.newPage();

// Add your new data / text here
// for example...
            try {
//            document.add(new Paragraph("my timestamp"));

                Bitmap bmp = BitmapFactory.decodeFile(photo.getAbsolutePath());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

                Image image = Image.getInstance(stream.toByteArray());
//            document.add(image);

                // get input stream
//            InputStream ims = getAssets().open("myImage.png");
//            Bitmap bmp = BitmapFactory.decodeStream(newBitmap);
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            Image image = Image.getInstance(stream.toByteArray());
//            document.add(image);


            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }

        document.close();



//        PdfDocument pdfDoc = new PdfDocument(new PdfReader(src.toString()), new PdfWriter(dest));
//
//        PdfDocument document = new PdfDocument(new PdfReader(String.valueOf(src)), new PdfWriter(dest));
//        if (initial == true)
//        {
//            document.addNewPage();
//        }
//        int pageCount = document.getNumberOfPages();
//
//
//        document.close();

        //sampe sini udah bener .. bisa addnewpage .. dan saved


//        PdfReader reader = new PdfReader(myFile.getAbsolutePath());
//        Path signfile = null;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            signfile = Files.createTempFile("sign", ".pdf");
//        }
//        FileOutputStream os = null;
////        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            os = new FileOutputStream(src);
////        }
//        PdfSigner signer = new PdfSigner(reader, os, false);

        //2nd PDF making
        File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), "tandatangandua");

        if (!pdfFolder.exists()) {
            pdfFolder.mkdir();
            Log.i(TAG, "Pdf Directory created");
            Log.i(TAG, "Pdf Directory created" +pdfFolder.mkdir());
        }

        //Create time stamp
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);

        File myFile2 = new File(pdfFolder + timeStamp + ".pdf");
        Log.d(TAG, "myFile2 :" +myFile2);

        outputStream2 = null;
        try {
            outputStream2 = new FileOutputStream(myFile2);
            Log.d(TAG, "outputStream :" +outputStream2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//
//
        Log.d(TAG, "dest : "+dest);
        Log.d(TAG, "myFile : "+myFile);

//        FileOutputStream os = new FileOutputStream(dest.toString());



        Log.d(TAG, "src.getAbsolutePath() :" +src.getAbsolutePath());
        Log.d(TAG, "myFile.getAbsolutePath() :" +myFile.getAbsolutePath());


//        PdfReader reader2 = new PdfReader(myFile2.getName());
////        FileOutputStream os = new FileOutputStream(dest.toString());
////        PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
////        // Creating the appearance
////         PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
////         appearance.setReason(reason);
////         appearance.setLocation(location);
////
////        appearance.set(name);
//
//        // Creating the signature
////         ExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm, provider);
////         ExternalDigest digest = new BouncyCastleDigest();
////         MakeSignature.signDetached(appearance, digest, pks, chain,         null, null, null, 0, subfilter);
//
//
////        PdfSigner signer = new PdfSigner(new PdfReader(myFile.toString()), outputStream, true);
////        // Creating the appearance
////        if (initial == true)
////        {
////            signer.setCertificationLevel(PdfSigner.CERTIFIED_FORM_FILLING_AND_ANNOTATIONS);
////        }
////        PdfSignatureAppearance appearance = signer.getSignatureAppearance()
////                .setReason(reason)
////                .setLocation(location)
////                .setReuseAppearance(false);
////        Rectangle rect = new Rectangle(10, 400, 100, 100);
////
////        if (pageCount > 1) {
////            appearance.setPageRect(rect).setPageNumber(pageCount);
////            Log.d(TAG, "pageCount : "+pageCount);
////        } else if (pageCount == 1){
////            appearance.setPageRect(rect).setPageNumber(1);
////            Log.d(TAG, "pageCount : "+pageCount);
////        }
////
////        appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION);
////        appearance.setSignatureCreator()
////        signer.setFieldName(signer.getNewSigFieldName());
//        // Creating the signature
////        ExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm, provider);
//
//
//
//
////        ProviderDigest digest = new ProviderDigest(provider);
////        ExternalDigest digest = new BouncyCastleDigest();
//
////        signer.signDetached( digest, pks, chain, crlList, ocspClient, tsaClient, estimatedSize, subfilter);


        //2nd PDF making
//        PdfReader reader2 = new PdfReader(myFile.toString());
        PdfReader reader2 = new PdfReader(myFile.getAbsolutePath());
//        PdfReader reader2 = new PdfReader(getContentResolver().openInputStream(uri));

//        PdfReader reader = new PdfReader(getContentResolver().openInputStream(myFile));


//
        stamper = null;     // Creating the appearance
        try {
            stamper = PdfStamper.createSignature(reader2, outputStream2, '\0');
            Log.d(TAG, "stamper : " +stamper);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        bf = null; // set font
        try {
            bf = BaseFont.createFont(
                    BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        //loop on pages (1-based)
//        for (int i=1; i<= reader2.getNumberOfPages(); i++){

            // get object for writing over the existing content;
            // you can also use getUnderContent for writing in the bottom layer

//        for (int i=1; i<= reader2.getNumberOfPages(); i++) {
//            PdfContentByte over = stamper.getOverContent(i);
//
//
//            // write text
//            over.beginText();
//            over.setFontAndSize(bf, 10);    // set font and size
//            over.setTextMatrix(50, 50);   // set x,y position (0,0 is at the bottom left)
//            over.showText("I can write at page " + i);  // set text
//            over.endText();
//        }

            // draw a red circle
//            over.setRGBColorStroke(0xFF, 0x00, 0x00);
//            over.setLineWidth(5f);
//            over.ellipse(250, 450, 350, 550);
//            over.stroke();

        PdfContentByte over = stamper.getOverContent(1);

            Bitmap bmp = BitmapFactory.decodeFile(photo.getAbsolutePath());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

            try {
                Image image = Image.getInstance(stream.toByteArray());
//                image.setTransparency(10);

                image.setAbsolutePosition(100f, 100f);

                //to add image over/ on pdf file

//                try {
//                    over.addImage(image);
////                    PdfGState state = new PdfGState();
////                    state.setBlendMode(PdfGState.BM_MULTIPLY);
////                    over.closePathEoFillStroke();
//
//                } catch (DocumentException e) {
//                    e.printStackTrace();
//                }


            } catch (BadElementException e) {
                e.printStackTrace();
            }
//        }



        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        Log.d(TAG, "stamper : " +appearance);
        Log.d(TAG, "stamper : " +stamper.getSignatureAppearance());

        Log.d(TAG, "reason : " +reason);
        Log.d(TAG, "reason : " +location);

        appearance.setReason(reason);
        appearance.setLocation(location);



//        appearance.setVisibleSignature("sig");
        appearance.setContact("help@digisec.asia");
        appearance.setSignatureEvent(
                new PdfSignatureAppearance.SignatureEvent(){
                    public void getSignatureDictionary(PdfDictionary sig) {
                        sig.put(PdfName.NAME, new PdfString(alias));         }
                } );
//        appearance.setSignatureCreator(alias);
//        appearance.getSignatureCreator();
//        appearance.getFieldName();

//        PdfSignature pdfSignature = new PdfSignature(PdfName.FILTER, PdfName.SUBFILTER);
//        pdfSignature.setName(alias);




        try {

            if (photo.getAbsolutePath() != null) {
                Bitmap bmp2 = BitmapFactory.decodeFile(photo.getAbsolutePath());
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                bmp2.compress(Bitmap.CompressFormat.PNG, 100, stream2);

                Image image = Image.getInstance(stream.toByteArray());


                appearance.setSignatureGraphic(image);
//            appearance.setSignatureGraphic(Image.getInstance(photo.getAbsolutePath()));
                Log.d(TAG, " photo.getAbsolutePath() : " + photo.getAbsolutePath());
                Log.d(TAG, " photo.getAbsolutePath() : " + photo.getName());
            }else {
                appearance.setSignatureCreator("Creator");
            }
        } catch (BadElementException e) {
            e.printStackTrace();
        }

//        appearance.setVisibleSignature(new Rectangle(0, 672, 200, 792), 2, "first");
        appearance.setVisibleSignature(new Rectangle(36, 748, 144, 780), 1, "sig");

        //To display digital signature image
        appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION);

        appearance.setImageScale(100);

//        try {
//            stamper.close();
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }


//
//




//
//        // Creating the signature
////         ExternalSignature pks2 = new PrivateKeySignature(pk, digestAlgorithm, provider);
//
//
////        SignaturePolicyIdentifier signaturePolicy = new SignaturePolicyIdentifier();
//
//
////        PrivateKey privateKey = null;
////        privateKey = KeyChain.getPrivateKey(getApplicationContext(), alias);
//
////        KeyChain.choosePrivateKeyAlias(this,
////                new KeyChainAliasCallback() {
////                    public void alias(String alias) {
////                        // Credential alias selected.  Remember the alias selection for future use.
////                        if (alias != null) {
////                            Handler h = new Handler(Looper.getMainLooper());
////                            h.post(new Runnable() {
////                                public void run() {
//////                                    showProgressDialog();
////                                }
////                            });
//////                            sign(alias, mReasonEditText.getText().toString(), mLocationEditText.getText().toString());
////                        }
////                    }
////                }, null, null, null, -1, null);
//
////        try {
////            privateKey = KeyChain.getPrivateKey(getApplicationContext(), alias);
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        } catch (KeyChainException e) {
////            e.printStackTrace();
////        }
//
//
////        KeyFactory keyFactory =
////                KeyFactory.getInstance(privateKey.getAlgorithm(), "AndroidKeyStore");
////
////
////        Certificate[] chain2 = new Certificate[0];
////        try {
////            chain2 = KeyChain.getCertificateChain(getApplicationContext(), alias);
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        } catch (KeyChainException e) {
////            e.printStackTrace();
////        }
//
        org.spongycastle.jce.provider.BouncyCastleProvider provider2 = new org.spongycastle.jce.provider.BouncyCastleProvider();
        Security.addProvider(provider2);

//        ExternalSignature pks = new PrivateKeySignature(pk, com.itextpdf.text.pdf.security.DigestAlgorithms.SHA256, provider2.getName());
        ExternalSignature pks2 = new CustomPrivateKeySignature(pk, com.itextpdf.text.pdf.security.DigestAlgorithms.SHA256, provider2.getName());

        Log.d(TAG,"externalSignature.getEncryptionAlgorithm() :" +pks2.getEncryptionAlgorithm());
        ExternalDigest digest = new BouncyCastleDigest();
//        ProviderDigest digest = new ProviderDigest(provider);

//        ExternalSignature pks = new CustomPrivateKeySignature(privateKey, com.itextpdf.text.pdf.security.DigestAlgorithms.SHA256, provider.getName());


//        ASN1InputStream bIn = new ASN1InputStream(new ByteArrayInputStream(rawBytes));

        SignaturePolicyIdentifier signaturePolicy = new SignaturePolicyIdentifier();

        String hashAlgorithm = pks2.getHashAlgorithm();
        com.itextpdf.text.pdf.security.PdfPKCS7 sgn = new PdfPKCS7((PrivateKey)pk,
                chain, hashAlgorithm, (String)null, digest, false);
        if(signaturePolicy != null) {
            sgn.setSignaturePolicy(signaturePolicy);
        }


//        TSAClient tsaClient2 =  new TSAClientBouncyCastle("http://tsa.rootca.or.id/", "", "");
//        tsaClient2.getTokenSizeEstimate();
//        tsaClient2.getMessageDigest();

        TSAClient tsaClient2 =  new TSAClientBouncyCastle("https://freetsa.org/tsr", "", "");
        tsaClient2.getTokenSizeEstimate();
        tsaClient2.getMessageDigest();
//        tsaClient2.getTimeStampToken();

        OCSPVerifier ocspVerifier = new OCSPVerifier(null, null);
        OcspClient ocsp = new OcspClientBouncyCastle();

//

//        byte[] digest2 = new byte[0];
//
//        ExternalDigest digest2 = new BouncyCastleDigest();

//        InputStream data = getRangeStream();
//
//        byte[] buf = new byte[4096];
//        int n;
//        while ((n = data.read(buf)) > 0) {
//            tsaClient2.getMessageDigest().update(buf, 0, n);
//        }
//
//        byte[] tsImprint = messageDigest.digest();
//        byte[] tsToken;
//        try {
//            tsToken = tsaClient2.getTimeStampToken(tsImprint);
//            tsaClient2.getTimeStampToken(tsToken);
//        } catch (Exception e) {
//            throw new GeneralSecurityException(e.getMessage(), e);
//        }
//
//
//
//        try {
//            LtvTimestamp.timestamp(appearance, tsaClient2, null);
//
////            LtvTimestamp.timestamp(appearance, tsaClient2, null);
//
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            LtvTimestamp.timestamp(appearance, tsaClient2, null);
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        Log.d(TAG, "tsaClient2 : " +tsaClient2);
//
        try {
            CustomMakeSignature.signDetached2(appearance, digest, pks2, chain,
                    null, ocsp, tsaClient2, 0, subfilter);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

//        hideProgressDialog();

        statusTextView.setText("Successfully wrote PDF to " + myFile2.getAbsolutePath());
        Log.d(TAG, "myFile2" +myFile2);
        Log.d(TAG, "myFile2" +myFile2.getAbsolutePath());

        openPDFsaved.setVisibility(View.VISIBLE);

        openPDFsaved.setEnabled(true);

        openPDFsaved.setOnClickListener(view ->{


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(myFile2), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);


        });




//        try {
//            MakeSignature.signDetached(appearance, digest, pks, chain,null, null,
//                    null, 0, subfilter);
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }

//        document.close();



        // Creating the reader and the stamper
//        com.itextpdf.text.pdf.PdfReader reader = new com.itextpdf.text.pdf.PdfReader(getContentResolver().openInputStream(Uri.fromFile(src)));
//
//        PdfStamper stamper = null;
//        try {
//            stamper = PdfStamper.createSignature(reader, outputStream2, '\0');
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        }
//        // Creating the appearance
//        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
//        appearance.setReason(reason);
//        appearance.setLocation(location);
//        appearance.setVisibleSignature(new Rectangle(36, 748, 144, 780), 1, "sig");
////        appearance.setImage(Image.getInstance(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/image.png"));
//
//        appearance.setImageScale(-1);
//        // Creating the signature
//        ExternalDigest digest = new BouncyCastleDigest();
//
//        CustomMakeSignature.signDetached(appearance, digest, pk, chain, null, null, null, 0, subfilter);
    }

//    protected InputStream getRangeStream() throws IOException {
//        RandomAccessSourceFactory fac = new RandomAccessSourceFactory();
//        return new RASInputStream(fac.createRanged(getUnderlyingSource(), range));
//    }
//
//    protected IRandomAccessSource getUnderlyingSource() throws IOException {
//        RandomAccessSourceFactory fac = new RandomAccessSourceFactory();
//        return raf == null ? fac.createSource(bout) : fac.createSource(raf);
//    }

    protected void saveAssetFile(String name, String path) throws IOException {
        // save pdf from assets
        InputStream is = getAssets().open(name);
        byte[] data = new byte[is.available()];
        is.read(data);
        is.close();

        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path));
        bos.write(data);
        bos.flush();
        bos.close();
    }

//    private void savePDF (String imageFile, String outputPath){
//
//        //IF THIS ONE'S RITE , MAK ALERT DIALOG EDITTEXT TO GET NEW FILE NAME
//
//        try {
//            // Check if Jpg file exists or not
//
//            File inputFile = new File(imageFile);
//            if (!inputFile.exists()) throw new Exception("File '" + imageFile + "' doesn't exist.");
//
//            // Create output file if needed
//            File outputFile = new File(outputPath);
//            if (!outputFile.exists()) outputFile.createNewFile();
//
//            Document document;
////            if (sharedPref.getString ("rotateString", "portrait").equals("portrait")) {
//            document = new Document(PageSize.A4);
////            } else {
////                document = new Document(PageSize.A4.rotate());
////            }
//
//            PdfWriter.getInstance(document, new FileOutputStream(outputFile));
//            document.open();
//
////            File imgFile = new File(Environment.getExternalStorageDirectory() + "/Pictures/.pdf_temp/pdf_temp.jpg");
////            if(imgFile.exists()){
////                imgFile.delete();
////            }
////            return true;
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
////        Document doc = new Document();
////        String root = Environment.getExternalStorageDirectory().toString();
////        Log.d(TAG, "root : " +root);
////        Log.d(TAG, "root : " +root);
////        File myDir = new File(imageFile.getAbsolutePath() + "/saved_pdf");
////        if (!myDir.exists()) {
////            myDir.mkdirs();
////            Toast.makeText(this, "Directory saved_pdf Created !" , Toast.LENGTH_LONG).show();
////        }
////        Random generator = new Random();
////        int n = 10000;
////        n = generator.nextInt(n);
////        String fname = "New-PDF-"+ n +".pdf";
////        File file = new File (myDir, fname);
////        if (file.exists ())
////            file.delete ();
////        try {
////            FileOutputStream out = new FileOutputStream(file);
//////            finalBitmap.compress(CompressFormat.PDF, 90, out);
////            PdfWriter.getInstance(doc, out);
//////            createPDF();
////
////
////
////
////            out.flush();
////            out.close();
////
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
////        File newxmlfile = new File(Environment.getExternalStorageDirectory() + "/Message.pdf");
//
////        Document document = new Document();
////        try {
////            PdfWriter.getInstance(document, new FileOutputStream(outputPath));
////        } catch (DocumentException e) {
////            e.printStackTrace();
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////        }
////        document.open();
////        Image image = null;
////        try {
////            image = Image.getInstance(imagePath);
////        } catch (BadElementException e) {
////            e.printStackTrace();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////        //document.setPageSize( new Rectangle(image.getAbsoluteX(), ));
////        try {
////            document.add(image);
////        } catch (DocumentException e) {
////            e.printStackTrace();
////        }
////        document.close();
//    }

    private class SavingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            buttonText.setEnabled(false);
//            buttonText.setText(R.string.downloadStarted);//Set Button Text when download started
        }

//        @Override
//        protected void onPostExecute(Void result) {
////            try {
////                if (outputFile != null) {
//////                    buttonText.setEnabled(true);
//////                    buttonText.setText(R.string.downloadCompleted);//If Download completed then change button text
////                } else {
//////                    buttonText.setText(R.string.downloadFailed);//If download failed change button text
////                    new Handler().postDelayed(new Runnable() {
////                        @Override
////                        public void run() {
//////                            buttonText.setEnabled(true);
//////                            buttonText.setText(R.string.downloadAgain);//Change button text again after 3sec
////                        }
////                    }, 3000);
////
////                    Log.e(TAG, "Download Failed");
////
////                }
////            } catch (Exception e) {
////                e.printStackTrace();
////
////                //Change button text if exception occurs
//////                buttonText.setText(R.string.downloadFailed);
////                new Handler().postDelayed(new Runnable() {
////                    @Override
////                    public void run() {
//////                        buttonText.setEnabled(true);
//////                        buttonText.setText(R.string.downloadAgain);
////                    }
////                }, 3000);
////                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());
////
////            }
//
//
//            super.onPostExecute(result);
//        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
//                URL url = new URL(downloadUrl);//Create Download URl
//                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
//                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
//                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
//                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
//                            + " " + c.getResponseMessage());
//
//                }


                //Get File if SD card is present
                if (new CheckForSDCard().isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + "/saved_pdf2");
                } else
                    Toast.makeText(OpenAndSavePDFActivity.this, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

//                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
//                while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);//Write new file
//                }

                //Close all connection after doing task
                fos.close();
//                fos.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }

    //    private static void setStrokeColor(PdfPage page, int color) {
//        page.setRGBStroke(
//                Color.red(color) / (float) 0xff,
//                Color.green(color) / (float) 0xff,
//                Color.blue(color) / (float) 0xff);
//    }
//
    private void showToast(String text, int duration) {
        Toast.makeText(this, text, duration).show();
    }





    public File getPublicAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
//        File file = new File(Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES), albumName);
//        if (!file.mkdirs()) {
//            Log.e(TAG, "Directory not created");
//        }
//        return file;

        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(pdfFileName , Context.MODE_PRIVATE);
            outputStream.write(albumName.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;

    }


    private File getTempFile(Context context, String url) {
        File file = null;
        try {
            String fileName = Uri.parse(url).getLastPathSegment();
            file = File.createTempFile(fileName, null, context.getCacheDir());
        } catch (IOException e) {
            // Error while creating file
        }
        return file;
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        Log.e(TAG, "title = " + meta.getTitle());
        Log.e(TAG, "author = " + meta.getAuthor());
        Log.e(TAG, "subject = " + meta.getSubject());
        Log.e(TAG, "keywords = " + meta.getKeywords());
        Log.e(TAG, "creator = " + meta.getCreator());
        Log.e(TAG, "producer = " + meta.getProducer());
        Log.e(TAG, "creationDate = " + meta.getCreationDate());
        Log.e(TAG, "modDate = " + meta.getModDate());

        printBookmarksTree(pdfView.getTableOfContents(), "-");
//        PdfDocument.Meta meta = pdfView.getDocumentMeta();
//        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

//    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
//        for (PdfDocument.Bookmark b : tree) {
//
//            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
//
//            if (b.hasChildren()) {
//                printBookmarksTree(b.getChildren(), sep + "-");
//            }
//        }
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        openPDFsaved.setVisibility(View.GONE);
        openPDFsaved.setEnabled(false);
    }
}

