package id.loginkuid.absensi.absensiDashboard;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.loginkuid.absensi.R;

public class MainAbsensiActivity2 extends AppCompatActivity {

    private static final String TAG = "login_activity";
    EditText PhoneNumber, Password;

    Button sendLocationtbtn, takeSelfieBtn;
    String PasswordHolder, PhoneHolder;
    String finalResult ;
    String HttpURL = "http://www.yukmarry.com/smartabakidaba/UserLogin.php";
    Boolean CheckEditText ;
    ProgressDialog progressDialog;
    HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    public static final String UserPhone = "";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String Addressextra = "address";
    public static final String datetimeextra = "datetime";

    String P_Number_Holder, Latitude_Holder , Longitude_Holder, Address_Holder, DateTime_Holder,
            city_Address_Holder, state_Address_Holder, country_Address_Holder, postalCode_Address_Holder,knownName_Address_Holder;
    //String lat = "", lon = "";
    //String address, city, currentDateTimeString;

    String HttpURLLocation = "http://www.yukmarry.com/absensi/SendLocationBener.php";

    String HttpURLCompleteName = "http://www.yukmarry.com/absensi/GetSpecificUserData.php";

    TextView tanggalTV, alamat;

    public static final String MY_PREFS = "SharedPreferences";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dashboard2);

        sendLocationtbtn = findViewById(R.id.sendLocationtbtn);
        tanggalTV = findViewById(R.id.tanggalTV);
        alamat = findViewById(R.id.alamat);



        sendLocationtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShowUserLoginCurrentLocation();

            }
        });



    }

    private void ShowUserLoginCurrentLocation() {
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) MainAbsensiActivity2.this.getSystemService(Context.LOCATION_SERVICE);
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {


                // Called when a new location is found by the network location provider.
                String lat = Double.toString(location.getLatitude());
                String lon = Double.toString(location.getLongitude());

                //String LatitudeHolder = txtLoc.getText().toString();
                //String LongitudeHolder =
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(MainAbsensiActivity2.this, Locale.getDefault());

                double latitude= Double.parseDouble(lat);
                double longitude= Double.parseDouble(lon);

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                    //txtAddress.setText("Alamat dari latitude longitude :"  + address + " ," + city + " ," + country);

                    //Date currentTime = Calendar.getInstance().getTime();
                    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

                    //tvDateTime.setText("Waktu Login : " + currentDateTimeString);
                    P_Number_Holder = PhoneNumber.getText().toString();
                    Latitude_Holder = lat;
                    Longitude_Holder = lon;
                    Address_Holder = address;
                    //blum di pass
                    city_Address_Holder = city;
                    state_Address_Holder = state;
                    country_Address_Holder = country;
                    postalCode_Address_Holder = postalCode;
                    knownName_Address_Holder = knownName;

                    Log.d(TAG, "Longitude_Holder 1 : " + Latitude_Holder );
                    Log.d(TAG, "Longitude_Holder 2 : " + Longitude_Holder );
                    Log.d(TAG, "Longitude_Holder 3 : " + city_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 4 : " + state_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 5 : " +country_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 6 : " + postalCode_Address_Holder );
                    Log.d(TAG, "Longitude_Holder 7 : " + knownName_Address_Holder );

                    DateTime_Holder = currentDateTimeString;

                    alamat.setText(Address_Holder);
                    tanggalTV.setText(DateTime_Holder);



                    //method to pass String above
                    UserSendLocationtoServerFunction(P_Number_Holder, Latitude_Holder , Longitude_Holder, Address_Holder, DateTime_Holder);


                } catch (IOException e) {
                    // print ke log jika Error
                    e.printStackTrace();
                }


            }

            public void onStatusChanged(String provider, int status, Bundle extras) { }
            public void onProviderEnabled(String provider) { }
            public void onProviderDisabled(String provider) { }
        };

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(MainAbsensiActivity2.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(MainAbsensiActivity2.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

    }

    public void UserSendLocationtoServerFunction( final String P_number, final String Latitude, final String Longitude, final String Address,final String DateTime){

        class UserSendLocationtoServerClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog = ProgressDialog.show(MainAbsensiActivity2.this,"Loading Data",null,true,true);
            }

            @Override
            protected void onPostExecute(String httpResponseMsg) {

                super.onPostExecute(httpResponseMsg);

                progressDialog.dismiss();

                finish();

                Intent intent = new Intent(MainAbsensiActivity2.this, SentAllActivity.class);
/*
                    intent.putExtra(latitude,Latitude);
                    intent.putExtra(longitude,Longitude);
                    intent.putExtra(Addressextra,Address);
                    intent.putExtra(datetimeextra,DateTime);
*/
                startActivity(intent);
                finish();


                Toast.makeText(MainAbsensiActivity2.this,httpResponseMsg.toString(), Toast.LENGTH_LONG).show();

            }

            @Override
            protected String doInBackground(String... params) {

//                hashMap.put("P_number",params[0]);

                hashMap.put("Latitude",params[0]);

                hashMap.put("Longitude",params[1]);

                hashMap.put("Address",params[2]);

                hashMap.put("DateTime",params[3]);

                finalResult = httpParse.postRequest(hashMap, HttpURLLocation);

                return finalResult;
            }
        }

        UserSendLocationtoServerClass userSendLocationtoServerClass = new UserSendLocationtoServerClass();

        userSendLocationtoServerClass.execute(P_number,Latitude,Longitude,Address,DateTime);
    }
}
