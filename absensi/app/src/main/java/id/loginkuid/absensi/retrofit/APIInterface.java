package id.loginkuid.absensi.retrofit;


//import com.imobilenetid.rizqmay.dpmdishub2.Model.dpm.activity.ResponseMatraUPTDPMbytime;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIInterface {



    @FormUrlEncoded
    @POST("UserLogin2.php")
    Call<ResponseBody> loginRequest(@Field("email") String email,
                                    @Field("password") String password);

    // Fungsi ini untuk memanggil API
    @FormUrlEncoded
    @POST("UserRegistration.php")
    Call<ResponseBody> registerRequest(@Field("nama") String nama,
                                       @Field("email") String email,
                                       @Field("password") String password);

}
