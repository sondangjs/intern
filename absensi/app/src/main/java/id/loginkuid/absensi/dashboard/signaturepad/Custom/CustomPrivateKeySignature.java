package id.loginkuid.absensi.dashboard.signaturepad.Custom;

import android.util.Log;

import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalSignature;

import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;

/**
 * Created by AnhVu on 5/10/17.
 *
 * Custom the PrivateKeySignature class of Itext to adapt with Android environment.
 */

public class CustomPrivateKeySignature implements ExternalSignature {
    private static final String TAG = "custom-privatekey";
    private PrivateKey pk;
    private String hashAlgorithm;
    private String encryptionAlgorithm;
    private String provider;

    public CustomPrivateKeySignature(PrivateKey pk, String hashAlgorithm, String provider) {
        this.pk = pk;
        Log.d(TAG, "pk.getAlgorithm() : "+pk);

        this.provider = provider;

        Log.d(TAG, "pk.getAlgorithm() : "+provider);
        this.hashAlgorithm = DigestAlgorithms.getDigest(DigestAlgorithms.getAllowedDigests(hashAlgorithm));
        this.encryptionAlgorithm = pk.getAlgorithm();
        Log.d(TAG, "pk.getAlgorithm() : "+pk.getAlgorithm());
        if(this.encryptionAlgorithm.startsWith("RSA")) {
            this.encryptionAlgorithm = "ECDSA";
        }

    }

    public String getHashAlgorithm() {
        return this.hashAlgorithm;
    }

    public String getEncryptionAlgorithm() {
        return this.encryptionAlgorithm;
    }

    public byte[] sign(byte[] b) throws GeneralSecurityException {
        String signMode = this.hashAlgorithm + "with" + this.encryptionAlgorithm;
        Signature sig;
        sig = Signature.getInstance("SHA256withRSA");
//        sig = Signature.getInstance(signMode);

        sig.initSign(this.pk);
        sig.update(b);
        return sig.sign();
    }
}
